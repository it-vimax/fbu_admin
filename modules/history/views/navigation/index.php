<?php
use yii\helpers\Html;
?>

<h2>Спарсеная база</h2>
<div class="row">
    <div class="col-md-4">
        <h4>Персонал</h4>
        <?= Html::a("Тренера", ['game/pars-all'], ['class' => ["btn", "btn-success"]]) ?>
        <br>
        <br>
        <?= Html::a("Дополнительная информация об тренере", ['game/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
        <br>
        <br>
        <?= Html::a("Комисары", ['game/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
        <br>
        <br>
        <?= Html::a("Арбитры", ['game/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
        <br>
        <br>
        <?= Html::a("Арбитры в играх", ['game/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
    <div class="col-md-4">
        <h4>Лиги / сезоны</h4>
        <?= Html::a("Лиги", ['game/pars-all'], ['class' => ["btn", "btn-success"]]) ?>
        <br>
        <br>
        <?= Html::a("Сезоны", ['game/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
    <div class="col-md-4">
        <h4>Игроки</h4>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a("Все игроки", ['league/pars-all'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a("Карьера игроков", ['season/all'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a("Сезоны игроков", ['season/all'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a("Рекорды игроков", ['season/all'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <h4>Команды</h4>
        <?= Html::a("Все команды", ['game/pars-all'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
    <div class="col-md-4">
        <h4>Игры</h4>
        <?= Html::a("Все игры", ['game/pars-all'], ['class' => ["btn", "btn-success"]]) ?>
        <br>
        <br>
        <?= Html::a("Дополнительная информация по играх", ['game/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
        <br>
        <br>
        <?= Html::a("Протокол по играх", ['game/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
        <br>
        <br>
        <?= Html::a("Протокол команд по играх", ['game/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
        <br>
        <br>
        <?= Html::a("Сравнительная статистика по играх", ['game/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
    <div class="col-md-4">
        <h4>Настройки паосинга</h4>
        <div class="row">
            <div class="col-md-12">
                <p>Сезоны, которые не проходять стандартный парсинг протокола игры</p>
                <?= Html::a("Настроить", ['league/pars-all'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
    </div>
</div>
<hr>
<!-- **************************************************************** парсинг *************************************************** -->
<h2>Парсинг FBU.UA</h2>
<div class="row">
    <div class="col-md-4">
        <h4>Парсинг персоначальных данных</h4>
        <?= Html::a("Начать", ['automat/first-data'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
    <div class="col-md-4">
        <h4>Парсинг игр</h4>
        <?= Html::a("Парсинг игр", ['game/pars-all'], ['class' => ["btn", "btn-success"]]) ?>
        <br>
        <br>
        <?= Html::a("Парсинг каждой игры подробно", ['game/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
    <div class="col-md-4">
        <h4>Парсинг лиг и сезонов</h4>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a("Создать лигы", ['league/pars-all'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a("Парсинг всех сезонов", ['season/all'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <h4>Парсинг персонала</h4>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a("Парсинг тренеров", ['personnel/trainer-all'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a("Парсинг комисаров", ['personnel/commissar-all'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a("Парсинг арбитров", ['personnel/judiciary-all'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <h4>Парсинг игроков</h4>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a("Парсинг игроков", ['player/pars-all'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <?= Html::a("Парсинг игрока", ['player/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <h4>Парсинг команд</h4>
        <div class="row">
            <div class="col-md-12">
                <?=Html::a("Все команды", ['teams/all-teams'], ['class' => ["btn", "btn-success"]]) ?>
            </div>
        </div>
    </div>
</div>
