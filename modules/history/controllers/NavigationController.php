<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 29.12.2017
 * Time: 15:15
 */

namespace app\modules\history\controllers;

use app\modules\history\models\Games;
use yii\web\Controller;

class NavigationController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}