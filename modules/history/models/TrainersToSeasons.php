<?php

namespace app\modules\history\models;

use Yii;

/**
 * This is the model class for table "trainers_to_seasons".
 *
 * @property integer $trainer_id
 * @property integer $season_id
 * @property string $team_name
 *
 * @property Seasons $season
 * @property PersonnelTrainers $trainer
 */
class TrainersToSeasons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trainers_to_seasons';
    }

    public static function getDb()
    {
        return Yii::$app->db2;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trainer_id', 'season_id'], 'required'],
            [['trainer_id', 'season_id'], 'integer'],
            [['team_name'], 'string', 'max' => 255],
            [['season_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seasons::className(), 'targetAttribute' => ['season_id' => 'id']],
            [['trainer_id'], 'exist', 'skipOnError' => true, 'targetClass' => PersonnelTrainers::className(), 'targetAttribute' => ['trainer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trainer_id' => 'Trainer ID',
            'season_id' => 'Season ID',
            'team_name' => 'Team Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(Seasons::className(), ['id' => 'season_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainer()
    {
        return $this->hasOne(PersonnelTrainers::className(), ['id' => 'trainer_id']);
    }
}
