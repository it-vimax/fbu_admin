<?php

namespace app\modules\history\models;

use Yii;

/**
 * This is the model class for table "player_highs".
 *
 * @property integer $id
 * @property integer $player_id
 * @property integer $season_id
 * @property string $category_name
 * @property string $category_number
 * @property string $date
 * @property integer $position
 *
 * @property Players $player
 * @property Seasons $season
 */
class PlayerHighs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player_highs';
    }

    public static function getDb()
    {
        return Yii::$app->db2;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_id', 'season_id'], 'required'],
            [['player_id', 'season_id', 'position'], 'integer'],
            [['date'], 'safe'],
            [['category_name', 'category_number'], 'string', 'max' => 255],
            [['player_id'], 'exist', 'skipOnError' => true, 'targetClass' => Players::className(), 'targetAttribute' => ['player_id' => 'id']],
            [['season_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seasons::className(), 'targetAttribute' => ['season_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'player_id' => 'Player ID',
            'season_id' => 'Season ID',
            'category_name' => 'Category Name',
            'category_number' => 'Category Number',
            'date' => 'Date',
            'position' => 'Position',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(Players::className(), ['id' => 'player_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(Seasons::className(), ['id' => 'season_id']);
    }
}
