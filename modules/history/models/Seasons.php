<?php

namespace app\modules\history\models;

use Yii;

/**
 * This is the model class for table "seasons".
 *
 * @property int $id
 * @property int $league_id
 * @property string $name
 *
 * @property Games[] $games
 * @property PlayerCareers[] $playerCareers
 * @property PlayerHighs[] $playerHighs
 * @property PlayerPhotos[] $playerPhotos
 * @property PlayerToSeason[] $playerToSeasons
 * @property Leagues $league
 * @property SettingSeasonForGameProtocol[] $settingSeasonForGameProtocols
 * @property TrainersToSeasons[] $trainersToSeasons
 * @property PersonnelTrainers[] $trainers
 */
class Seasons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seasons';
    }

    public static function getDb()
    {
        return Yii::$app->db2;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['league_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['league_id'], 'exist', 'skipOnError' => true, 'targetClass' => Leagues::className(), 'targetAttribute' => ['league_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'league_id' => 'League ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Games::className(), ['season_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerCareers()
    {
        return $this->hasMany(PlayerCareers::className(), ['season_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerHighs()
    {
        return $this->hasMany(PlayerHighs::className(), ['season_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerPhotos()
    {
        return $this->hasMany(PlayerPhotos::className(), ['season_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerToSeasons()
    {
        return $this->hasMany(PlayerToSeason::className(), ['season_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeague()
    {
        return $this->hasOne(Leagues::className(), ['id' => 'league_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettingSeasonForGameProtocols()
    {
        return $this->hasMany(SettingSeasonForGameProtocol::className(), ['season_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainersToSeasons()
    {
        return $this->hasMany(TrainersToSeasons::className(), ['season_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainers()
    {
        return $this->hasMany(PersonnelTrainers::className(), ['id' => 'trainer_id'])->viaTable('trainers_to_seasons', ['season_id' => 'id']);
    }
}
