<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "game_rooted_statistics".
 *
 * @property integer $id
 * @property integer $game_id
 * @property integer $team_id
 * @property string $team_status
 * @property string $time_in_game
 * @property string $two_point_kiddies
 * @property string $two_point_kiddies_ok
 * @property string $three_point_kiddies
 * @property string $three_point_kiddies_ok
 * @property string $one_point_kiddies
 * @property string $one_point_kiddies_ok
 * @property string $picking_up_in_an_attack
 * @property string $picking_up_in_defense
 * @property string $overall_picking_up
 * @property string $transmissions
 * @property string $personal_foul
 * @property string $losses
 * @property string $interception
 * @property string $block_shots
 * @property string $efficiency
 * @property string $gained_points
 *
 * @property Games $game
 * @property Teams $team
 */
class GameRootedStatistics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game_rooted_statistics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'team_id'], 'integer'],
            [['team_status', 'time_in_game', 'two_point_kiddies', 'two_point_kiddies_ok', 'three_point_kiddies', 'three_point_kiddies_ok', 'one_point_kiddies', 'one_point_kiddies_ok', 'picking_up_in_an_attack', 'picking_up_in_defense', 'overall_picking_up', 'transmissions', 'personal_foul', 'losses', 'interception', 'block_shots', 'efficiency', 'gained_points'], 'string', 'max' => 255],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['game_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teams::className(), 'targetAttribute' => ['team_id' => 'team_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'team_id' => 'Team ID',
            'team_status' => 'Team Status',
            'time_in_game' => 'Time In Game',
            'two_point_kiddies' => 'Two Point Kiddies',
            'two_point_kiddies_ok' => 'Two Point Kiddies Ok',
            'three_point_kiddies' => 'Three Point Kiddies',
            'three_point_kiddies_ok' => 'Three Point Kiddies Ok',
            'one_point_kiddies' => 'One Point Kiddies',
            'one_point_kiddies_ok' => 'One Point Kiddies Ok',
            'picking_up_in_an_attack' => 'Picking Up In An Attack',
            'picking_up_in_defense' => 'Picking Up In Defense',
            'overall_picking_up' => 'Overall Picking Up',
            'transmissions' => 'Transmissions',
            'personal_foul' => 'Personal Foul',
            'losses' => 'Losses',
            'interception' => 'Interception',
            'block_shots' => 'Block Shots',
            'efficiency' => 'Efficiency',
            'gained_points' => 'Gained Points',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Games::className(), ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Teams::className(), ['team_id' => 'team_id']);
    }
}
