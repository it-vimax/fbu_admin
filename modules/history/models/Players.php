<?php

namespace app\modules\history\models;

use Yii;

/**
 * This is the model class for table "players".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $date_of_birth
 * @property string $position
 * @property int $height
 * @property int $weight
 *
 * @property GameProtocolCommands[] $gameProtocolCommands
 * @property PlayerCareers[] $playerCareers
 * @property PlayerHighs[] $playerHighs
 * @property PlayerPhotos[] $playerPhotos
 */
class Players extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'players';
    }

    public static function getDb()
    {
        return Yii::$app->db2;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_of_birth'], 'safe'],
            [['height', 'weight'], 'integer'],
            [['first_name', 'last_name', 'position'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'date_of_birth' => 'Date Of Birth',
            'position' => 'Position',
            'height' => 'Height',
            'weight' => 'Weight',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameProtocolCommands()
    {
        return $this->hasMany(GameProtocolCommands::className(), ['player_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerCareers()
    {
        return $this->hasMany(PlayerCareers::className(), ['player_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerHighs()
    {
        return $this->hasMany(PlayerHighs::className(), ['player_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerPhotos()
    {
        return $this->hasMany(PlayerPhotos::className(), ['player_id' => 'id']);
    }
}
