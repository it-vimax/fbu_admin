<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "game_protocol_team_comparisons".
 *
 * @property integer $id
 * @property integer $game_id
 * @property integer $team_status
 * @property integer $team_id
 * @property string $two_point_kiddies
 * @property string $two_point_kiddies_ok
 * @property string $two_point_kiddies_percent
 * @property string $three_point_kiddies
 * @property string $three_point_kiddies_ok
 * @property string $three_point_kiddies_percent
 * @property string $one_point_kiddies
 * @property string $one_point_kiddies_ok
 * @property string $one_point_kiddies_percent
 * @property string $picking_up_in_an_attack
 * @property string $picking_up_in_defense
 * @property string $overall_picking_up
 * @property string $transmissions
 * @property string $personal_foul
 * @property string $losses
 * @property string $interception
 * @property string $block_shots_1
 * @property string $efficiency
 * @property string $gained_points
 *
 * @property Games $game
 * @property Teams $team
 */
class GameProtocolTeamComparisons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game_protocol_team_comparisons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'team_status', 'team_id'], 'integer'],
            [['two_point_kiddies', 'two_point_kiddies_ok', 'two_point_kiddies_percent', 'three_point_kiddies', 'three_point_kiddies_ok', 'three_point_kiddies_percent', 'one_point_kiddies', 'one_point_kiddies_ok', 'one_point_kiddies_percent', 'picking_up_in_an_attack', 'picking_up_in_defense', 'overall_picking_up', 'transmissions', 'personal_foul', 'losses', 'interception', 'block_shots_1', 'efficiency', 'gained_points'], 'string', 'max' => 255],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['game_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teams::className(), 'targetAttribute' => ['team_id' => 'team_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'team_status' => 'Team Status',
            'team_id' => 'Team ID',
            'two_point_kiddies' => 'Two Point Kiddies',
            'two_point_kiddies_ok' => 'Two Point Kiddies Ok',
            'two_point_kiddies_percent' => 'Two Point Kiddies Percent',
            'three_point_kiddies' => 'Three Point Kiddies',
            'three_point_kiddies_ok' => 'Three Point Kiddies Ok',
            'three_point_kiddies_percent' => 'Three Point Kiddies Percent',
            'one_point_kiddies' => 'One Point Kiddies',
            'one_point_kiddies_ok' => 'One Point Kiddies Ok',
            'one_point_kiddies_percent' => 'One Point Kiddies Percent',
            'picking_up_in_an_attack' => 'Picking Up In An Attack',
            'picking_up_in_defense' => 'Picking Up In Defense',
            'overall_picking_up' => 'Overall Picking Up',
            'transmissions' => 'Transmissions',
            'personal_foul' => 'Personal Foul',
            'losses' => 'Losses',
            'interception' => 'Interception',
            'block_shots_1' => 'Block Shots 1',
            'efficiency' => 'Efficiency',
            'gained_points' => 'Gained Points',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Games::className(), ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Teams::className(), ['team_id' => 'team_id']);
    }
}
