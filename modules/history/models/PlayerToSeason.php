<?php

namespace app\modules\history\models;

use Yii;

/**
 * This is the model class for table "player_to_season".
 *
 * @property integer $player_id
 * @property integer $season_id
 * @property integer $club_id
 * @property integer $count_game
 * @property integer $points
 * @property integer $pink_up
 * @property integer $forwarding
 * @property integer $efficiency
 * @property integer $plus_minus
 *
 * @property Teams $club
 * @property Seasons $season
 */
class PlayerToSeason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player_to_season';
    }

    public static function getDb()
    {
        return Yii::$app->db2;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_id', 'season_id'], 'required'],
            [['player_id', 'season_id', 'club_id', 'count_game', 'points', 'pink_up', 'forwarding', 'efficiency', 'plus_minus'], 'integer'],
            [['club_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teams::className(), 'targetAttribute' => ['club_id' => 'team_id']],
            [['season_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seasons::className(), 'targetAttribute' => ['season_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'player_id' => 'Player ID',
            'season_id' => 'Season ID',
            'club_id' => 'Club ID',
            'count_game' => 'Count Game',
            'points' => 'Points',
            'pink_up' => 'Pink Up',
            'forwarding' => 'Forwarding',
            'efficiency' => 'Efficiency',
            'plus_minus' => 'Plus Minus',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClub()
    {
        return $this->hasOne(Teams::className(), ['team_id' => 'club_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(Seasons::className(), ['id' => 'season_id']);
    }
}
