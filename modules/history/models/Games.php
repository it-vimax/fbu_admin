<?php

namespace app\modules\history\models;

use Yii;

/**
 * This is the model class for table "games".
 *
 * @property integer $id
 * @property integer $season_id
 * @property string $date
 * @property string $number_game
 * @property string $group
 * @property string $step
 * @property integer $command_1
 * @property integer $command_2
 * @property integer $result_command_1
 * @property integer $result_command_2
 * @property string $arena
 * @property integer $commissar_id
 * @property string $viewers
 * @property integer $first_quarter_comand_1
 * @property integer $first_quarter_comand_2
 * @property integer $second_quarter_comand_1
 * @property integer $second_quarter_comand_2
 * @property integer $third_quarter_comand_1
 * @property integer $third_quarter_comand_2
 * @property integer $fourth_quarter_comand_1
 * @property integer $fourth_quarter_comand_2
 * @property integer $the_result_of_the_five_comand_1
 * @property integer $the_result_of_the_five_comand_2
 * @property integer $beat_the_spare_comand_1
 * @property integer $beat_the_spare_comand_2
 * @property integer $the_hottest_jerk_comand_1
 * @property integer $the_hottest_jerk_comand_2
 * @property integer $the_biggest_gap_comand_1
 * @property integer $the_biggest_gap_comand_2
 * @property integer $points_from_the_zone_comand_1
 * @property integer $points_from_the_zone_comand_2
 *
 * @property GameJudiciarys[] $gameJudiciarys
 * @property GameOtherStatistic[] $gameOtherStatistics
 * @property GameProtocolCommands[] $gameProtocolCommands
 * @property GameProtocolTeamComparisons[] $gameProtocolTeamComparisons
 * @property GameRootedStatistics[] $gameRootedStatistics
 * @property Teams $command1
 * @property Teams $command2
 * @property PersonnelCommissars $commissar
 * @property Seasons $season
 */
class Games extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'games';
    }

    public static function getDb()
    {
        return Yii::$app->db2;
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['season_id'], 'required'],
            [['season_id', 'command_1', 'command_2', 'result_command_1', 'result_command_2', 'commissar_id', 'first_quarter_comand_1', 'first_quarter_comand_2', 'second_quarter_comand_1', 'second_quarter_comand_2', 'third_quarter_comand_1', 'third_quarter_comand_2', 'fourth_quarter_comand_1', 'fourth_quarter_comand_2', 'the_result_of_the_five_comand_1', 'the_result_of_the_five_comand_2', 'beat_the_spare_comand_1', 'beat_the_spare_comand_2', 'the_hottest_jerk_comand_1', 'the_hottest_jerk_comand_2', 'the_biggest_gap_comand_1', 'the_biggest_gap_comand_2', 'points_from_the_zone_comand_1', 'points_from_the_zone_comand_2'], 'integer'],
            [['date'], 'safe'],
            [['number_game', 'group', 'step', 'arena', 'viewers'], 'string', 'max' => 255],
            [['command_1'], 'exist', 'skipOnError' => true, 'targetClass' => Teams::className(), 'targetAttribute' => ['command_1' => 'team_id']],
            [['command_2'], 'exist', 'skipOnError' => true, 'targetClass' => Teams::className(), 'targetAttribute' => ['command_2' => 'team_id']],
            [['commissar_id'], 'exist', 'skipOnError' => true, 'targetClass' => PersonnelCommissars::className(), 'targetAttribute' => ['commissar_id' => 'id']],
            [['season_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seasons::className(), 'targetAttribute' => ['season_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'season_id' => 'Season ID',
            'date' => 'Date',
            'number_game' => 'Number Game',
            'group' => 'Group',
            'step' => 'Step',
            'command_1' => 'Command 1',
            'command_2' => 'Command 2',
            'result_command_1' => 'Result Command 1',
            'result_command_2' => 'Result Command 2',
            'arena' => 'Arena',
            'commissar_id' => 'Commissar ID',
            'viewers' => 'Viewers',
            'first_quarter_comand_1' => 'First Quarter Comand 1',
            'first_quarter_comand_2' => 'First Quarter Comand 2',
            'second_quarter_comand_1' => 'Second Quarter Comand 1',
            'second_quarter_comand_2' => 'Second Quarter Comand 2',
            'third_quarter_comand_1' => 'Third Quarter Comand 1',
            'third_quarter_comand_2' => 'Third Quarter Comand 2',
            'fourth_quarter_comand_1' => 'Fourth Quarter Comand 1',
            'fourth_quarter_comand_2' => 'Fourth Quarter Comand 2',
            'the_result_of_the_five_comand_1' => 'The Result Of The Five Comand 1',
            'the_result_of_the_five_comand_2' => 'The Result Of The Five Comand 2',
            'beat_the_spare_comand_1' => 'Beat The Spare Comand 1',
            'beat_the_spare_comand_2' => 'Beat The Spare Comand 2',
            'the_hottest_jerk_comand_1' => 'The Hottest Jerk Comand 1',
            'the_hottest_jerk_comand_2' => 'The Hottest Jerk Comand 2',
            'the_biggest_gap_comand_1' => 'The Biggest Gap Comand 1',
            'the_biggest_gap_comand_2' => 'The Biggest Gap Comand 2',
            'points_from_the_zone_comand_1' => 'Points From The Zone Comand 1',
            'points_from_the_zone_comand_2' => 'Points From The Zone Comand 2',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameJudiciarys()
    {
        return $this->hasMany(GameJudiciarys::className(), ['game_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameOtherStatistics()
    {
        return $this->hasMany(GameOtherStatistic::className(), ['game_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameProtocolCommands()
    {
        return $this->hasMany(GameProtocolCommands::className(), ['game_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameProtocolTeamComparisons()
    {
        return $this->hasMany(GameProtocolTeamComparisons::className(), ['game_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameRootedStatistics()
    {
        return $this->hasMany(GameRootedStatistics::className(), ['game_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommand1()
    {
        return $this->hasOne(Teams::className(), ['team_id' => 'command_1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommand2()
    {
        return $this->hasOne(Teams::className(), ['team_id' => 'command_2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommissar()
    {
        return $this->hasOne(PersonnelCommissars::className(), ['id' => 'commissar_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(Seasons::className(), ['id' => 'season_id']);
    }
}
