<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "game_other_statistic".
 *
 * @property integer $id
 * @property integer $game_id
 * @property string $name
 * @property string $team_1
 * @property string $team_2
 *
 * @property Games $game
 */
class GameOtherStatistic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game_other_statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id'], 'integer'],
            [['name', 'team_1', 'team_2'], 'string', 'max' => 255],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['game_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'name' => 'Name',
            'team_1' => 'Team 1',
            'team_2' => 'Team 2',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Games::className(), ['id' => 'game_id']);
    }
}
