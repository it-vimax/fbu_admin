<?php

namespace app\modules\history\models;

use Yii;

/**
 * This is the model class for table "personnel_trainers".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 *
 * @property TrainersToSeasons[] $trainersToSeasons
 */
class PersonnelTrainers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'personnel_trainers';
    }

    public static function getDb()
    {
        return Yii::$app->db2;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainersToSeasons()
    {
        return $this->hasMany(TrainersToSeasons::className(), ['trainer_id' => 'id']);
    }
}
