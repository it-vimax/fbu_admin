<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\QuestionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'question') ?>

    <?= $form->field($model, 'fotos') ?>

    <?= $form->field($model, 'videolink') ?>

    <?= $form->field($model, 'date_quest') ?>

    <?php // echo $form->field($model, 'answer') ?>

    <?php // echo $form->field($model, 'user_answer_id') ?>

    <?php // echo $form->field($model, 'date_answer') ?>

    <?php // echo $form->field($model, 'answer_title') ?>

    <?php // echo $form->field($model, 'publish') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>