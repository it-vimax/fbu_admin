<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use dosamigos\datetimepicker\DateTimePicker;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Questions */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    img{
        width: 100%;
    }
</style>
<div class="questions-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="col-sm-9">
        <?= $form->field($model, 'question')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'videolink')->textInput(['maxlength' => true]) ?>
        <?php if(!!$model->fotos) {?>
            <div class="row">
                <?php $foto_links = json_decode($model->fotos); ?>
                <?php foreach($foto_links as $foto) {?>
                <div class="col-sm-4">
                    <img src="http://fbu.ua/web/<?=$foto?>" alt="">
                </div>
                <?php } ?>
            </div>
        <?php } ?>
        
        <?= $form->field($model, 'answer_title')->textInput(['maxlength' => true]) ?>
       
        <?= $form->field($model, 'answer')->widget(CKEditor::className(), [

                'options' => ['rows' => 6],
                'preset' => 'full',
                'clientOptions' => ElFinder::ckeditorOptions('elfinder',['filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news-id'=>'gameswidget']),]),
                
            ]) ?>
    
    </div>
    <div class="col-sm-3">
    <?= $form->field($model, 'date_quest')->widget(DateTimePicker::className(), [
                'language' => 'ru',
                'size' => 'ms',
                'template' => '{input}',
                'pickButtonIcon' => 'glyphicon glyphicon-time',
                'inline' => false,
                'clientOptions' => [
                    'startView' => 2,
                    'minView' => 0,
                    'maxView' => 3,
                    'autoclose' => true,
                    //'linkFormat' => 'HH:ii P', // if inline = true
                    'format' => 'yyyy-mm-dd HH:ii:ss', // if inline = false
                    'todayBtn' => true
                ]
            ]);?>
       
         <?= $form->field($model, 'publish')->dropDownList(['0'=>'Нет', '1'=>'Да'])?>    
         <?= $form->field($model, 'page')->dropDownList(['0'=>'Нет', '1'=>'Да'])?>
    </div>

    

   
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    


    <div class="hidden">
    <?= $form->field($model, 'fotos')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'date_answer')->textInput() ?>
    <?= $form->field($model, 'user_answer_id')->textInput() ?>
</div>


    <?php ActiveForm::end(); ?>

</div>