<?php

use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use app\modules\admin\models\Topic;
use yii\helpers\ArrayHelper;
$topics = Topic::find()->where(['siteid'=>1])->orderBy(['topicid'=>SORT_ASC])->asArray()->all();
$topics = ArrayHelper::map($topics, 'topicid', 'name');
?>

<div class="newstopic-form">
    <?php $count_top = count($newstopics); ?>
    <?= Html::button('Добавить тег', ['value' => Url::to(['news/addtag', 'index'=>$count_top, 'newsid'=>$newsid]), 'id' => 'add_topic', 'class'=>'btn btn-primary', ]); ?>
    <div class="row">
        <div class="col-sm-3 new hidden">
        </div>
        <?php if(is_array($newstopics)){ ?>
            <?php foreach ($newstopics as $index => $topic){ ?>
                <div class="col-sm-3">
                    <div class="hidden">
                        <?= $form->field($topic, "[$index]newsid")->textInput() ?>
                    </div>
                    <?= $form->field($topic, "[$index]topicid")->dropDownList($topics) ?>
                    <?= $form->field($topic, "[$index]sort_order")->textInput() ?>
                    <?= Html::button('Удалить тег', ['value' => Url::to(['news/deletetag', 'topicid'=>$topic->topicid, 'newsid'=>$newsid ]), 'id' => 'deletetag', 'class'=>'btn btn-primary', ]); ?>
                </div>
            <?php }  ?>
        <?php }  else { ?>
        <?php }?>
    </div>
    <div class="result2"></div>
</div>
<?php

$scripttopic = <<< JS

    jQuery('body').on('click', '#deletetag', function (e) {
        e.preventDefault();
        var el = jQuery(this);
         jQuery('.result2').load(jQuery(this).attr('value'), function() {
         el.parents('.col-sm-3').remove();
         });
         
        
    });
    jQuery('body').on('click', '#deletesessiontag', function (e) {
        var el = jQuery(this);
        el.parents('.col-sm-3').remove();
    })
    
    jQuery('body').on('click', '#add_topic', function (e) {
                e.preventDefault();
                jQuery('.newstopic-form .col-sm-3.new').load(jQuery(this).attr('value'), function() {               
                  jQuery(this).find('#one-topic').unwrap();
                 jQuery(this).removeClass('new');
                 jQuery(this).removeClass('hidden');
                  jQuery('.newstopic-form .row').prepend('<div class="col-sm-3 new hidden"></div>');  
                  
                });
                
            });
    
JS;
$this->registerJs($scripttopic, View::POS_READY); ?>
</div>