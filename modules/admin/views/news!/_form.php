<?php

use yii\helpers\Html;

use dosamigos\ckeditor\CKEditor;
use app\modules\admin\models\Section;
use yii\helpers\ArrayHelper;
use dosamigos\datetimepicker\DateTimePicker;
use mihaildev\elfinder\ElFinder;

$sections = Section::find()->where(['siteid'=>1])->asArray()->all();
$sections = ArrayHelper::map($sections, 'sectionid', 'name');

?>

<div class="news-form">
    <div class="row">
        <div class="col-sm-9">

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
            <?= $form->field($model, 'fulltext')->widget(CKEditor::className(), [

                'options' => ['rows' => 6],
                'preset' => 'full',
                'clientOptions' => ElFinder::ckeditorOptions('elfinder',['filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news-id'=>$model->newsid]),]),
                /*'clientOptions' => [
                    'filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news-id'=>$model->newsid]),
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder',[]),
                ]*/
            ]) ?>
            <div class="row">
                <div class="col-sm-6"><?= $form->field($model, 'sourcename')->textInput(['maxlength' => true]) ?></div>
                <div class="col-sm-6"><?= $form->field($model, 'sourceurl')->textInput() ?></div>
            </div>


        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'siteid')->dropDownList(['1'=>'ФБУ', '2'=>'3x3']) ?>
            <?= $form->field($model, 'sectionid')->dropDownList($sections) ?>
            <?= $form->field($model, 'importanceid')->dropDownList(['4'=>'нет', '2'=>'MainItem', '3'=>'Публикация']) ?>
            <div class="row">
                <div class="col-sm-6"><?= $form->field($model, 'videotype')->radioList([0 => 'Нет', 1 => 'Да']) ?></div>
                <div class="col-sm-6"><?= $form->field($model, 'fototype')->radioList([0 => 'Нет', 1 => 'Да']) ?></div>
            </div>
            <?= $form->field($model, 'isactive')->radioList([0 => 'Нет', 1 => 'Да'])?>
            <?= $form->field($model, 'date')->widget(DateTimePicker::className(), [
                'language' => 'ru',
                'size' => 'ms',
                'template' => '{input}',
                'pickButtonIcon' => 'glyphicon glyphicon-time',
                'inline' => false,
                'clientOptions' => [
                    'startView' => 2,
                    'minView' => 0,
                    'maxView' => 3,
                    'autoclose' => true,
                    //'linkFormat' => 'HH:ii P', // if inline = true
                    'format' => 'yyyy-mm-dd HH:ii:ss', // if inline = false
                    'todayBtn' => true
                ]
            ]);?>


            <?php echo $form->field($model, 'imagehorizontal')->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
                'aspectRatio' => (16/9), //set the aspect ratio
                'showPreview' => true, //false to hide the preview
                'showDeletePickedImageConfirm' => false, //on true show warning before detach image
                'news_id'=>$model->newsid,
            ]); ?>
            <?= $form->field($model, 'imagesourcename')->textInput() ?>
        </div>
    </div>
    <div class="hidden">
        <?= $form->field($model, 'edituserid')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'createdate')->textInput() ?>
        <?= $form->field($model, 'newsid')->textInput() ?>
        <?= $form->field($model, 'createuserid')->textInput() ?>
        <?= $form->field($model, 'oldurl')->textInput() ?>
        <?= $form->field($model, 'isremoved')->textInput() ?>
        <?= $form->field($model, 'url')->textInput()?>
        <?= $form->field($model, 'viewcount')->textInput() ?>
        <?= $form->field($model, 'editdate')->textInput() ?>
        <?= $form->field($model, 'updatedate')->textInput() ?>
        <?= $form->field($model, 'nurl')->textInput() ?>
    </div>

</div>
