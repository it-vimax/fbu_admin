<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'newsid',
            //'siteid',
            //'sectionid',
            //'importanceid',
            //'createuserid',
             'title',
            // 'link:ntext',
            // 'linkattributes:ntext',
            // 'oldurl:ntext',
            // 'description:ntext',
            // 'fulltext:ntext',
             'date',
            // 'sourcename',
            // 'sourceurl:ntext',
            // 'createdate',
            // 'isactive',
            // 'imagehorizontal',
            // 'edituserid',
            // 'editdate',
            // 'isremoved',
            // 'url:ntext',
            // 'imagesourcename:ntext',
            // 'viewcount',
            // 'titlefordisplay',
            // 'updatedate',
            // 'nurl:url',
            // 'videotype',
            // 'fototype',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
