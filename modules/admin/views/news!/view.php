<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->newsid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->newsid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'newsid',
            //'siteid',
            //'sectionid',
           // 'importanceid',
           // 'createuserid',
            'title',
           // 'oldurl:ntext',
            'description:ntext',
            'fulltext:ntext',
            'date',
           // 'sourcename',
           // 'sourceurl:ntext',
            'createdate',
           // 'isactive',
            //'imagehorizontal',
            //'edituserid',
            //'editdate',
            //'isremoved',
            //'url:ntext',
            //'imagesourcename:ntext',
           // 'viewcount',

           // 'updatedate',
            [
                'label' => 'Урл',
                'format'=>'url',

                'value' => function ($model) {
                   
                    return 'http://fbu.ua/news/'.$model->nurl;
                }
            ],
           
           
           // 'videotype',
            //'fototype',
        ],
    ]) ?>

</div>
