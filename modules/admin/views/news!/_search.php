<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'newsid') ?>

    <?= $form->field($model, 'siteid') ?>

    <?= $form->field($model, 'sectionid') ?>

    <?= $form->field($model, 'importanceid') ?>

    <?= $form->field($model, 'createuserid') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'link') ?>

    <?php // echo $form->field($model, 'linkattributes') ?>

    <?php // echo $form->field($model, 'oldurl') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'fulltext') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'sourcename') ?>

    <?php // echo $form->field($model, 'sourceurl') ?>

    <?php // echo $form->field($model, 'createdate') ?>

    <?php // echo $form->field($model, 'isactive') ?>

    <?php // echo $form->field($model, 'imagehorizontal') ?>

    <?php // echo $form->field($model, 'edituserid') ?>

    <?php // echo $form->field($model, 'editdate') ?>

    <?php // echo $form->field($model, 'isremoved') ?>

    <?php // echo $form->field($model, 'url') ?>

    <?php // echo $form->field($model, 'imagesourcename') ?>

    <?php // echo $form->field($model, 'viewcount') ?>

    <?php // echo $form->field($model, 'titlefordisplay') ?>

    <?php // echo $form->field($model, 'updatedate') ?>

    <?php // echo $form->field($model, 'nurl') ?>

    <?php // echo $form->field($model, 'videotype') ?>

    <?php // echo $form->field($model, 'fototype') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
