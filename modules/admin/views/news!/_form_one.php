<?php
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $form = ActiveForm::begin([]); ?>
    <div id="one-image">
    <div class="hidden">
        <?= $form->field($model, "[$index]newsimageid")->textInput() ?>
        <?= $form->field($model, "[$index]newsid")->textInput() ?>
        <?= $form->field($model, "[$index]position")->textInput(['class' => 'position change']) ?>
    </div>
    <?php echo $form->field($model, "[$index]filename")->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
        'aspectRatio' => (16/9), //set the aspect ratio
        'showPreview' => true, //false to hide the preview
        'showDeletePickedImageConfirm' => false, //on true show warning before detach image
        'news_id'=>$model->newsid.'/gallery',
    ]); ?>

    <?= $form->field($model, "[$index]title")->textInput() ?>

    <?= $form->field($model, "[$index]sourcename")->textInput() ?></div>
    <?= Html::button('Удалить картинку', ['value' => Url::to(['news/deleteImage']), 'id' => 'deletesession', 'class'=>'btn btn-primary', ]); ?>
<?php ActiveForm::end(); ?>
<?php unset($form);?>
