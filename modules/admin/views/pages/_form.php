<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\ElFinder;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
<?php if ($mode == 'update') {   ?><div class="hidden"><?php } ?>
    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
  <?php if ($mode == 'update') {   ?></div><?php } ?>  
    <?= $form->field($model, 'content')->widget(CKEditor::className(), [

                'options' => ['rows' => 6],
                'preset' => 'full',
                'clientOptions' => ElFinder::ckeditorOptions('elfinder',['filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news-id'=>'gameswidget']),]),
                /*'clientOptions' => [
                    'filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news-id'=>$model->newsid]),
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder',[]),
                ]*/
            ]) ?>


    
<div class="hidden">
    <?= $form->field($model, 'date_added')->textInput() ?>

    <?= $form->field($model, 'created_user')->textInput() ?>
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>