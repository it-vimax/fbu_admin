<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Banners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banners-form">

    <?php $form = ActiveForm::begin(); ?>
		<?php echo $form->field($model, "foto")->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
                        'aspectRatio' => (16/9), //set the aspect ratio
                        'showPreview' => true, //false to hide the preview
                        'showDeletePickedImageConfirm' => false, //on true show warning before detach image
                        'news_id'=>'gameswidget',
                    ]); ?>
   

    <?= $form->field($model, 'link')->textInput() ?>
    <?= $form->field($model, 'position')->dropDownList([0=>'Баннер справа', 1=>'Баннер вместо раздела ФОТО', 2=>'Background сайта']) ?>
	<div class="hidden">
    <?= $form->field($model, 'created_user')->textInput() ?>

    <?= $form->field($model, 'date_added')->textInput() ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>