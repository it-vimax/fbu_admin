<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'newsid',
            //'siteid',
            //'sectionid',
            //'importanceid',


             'title',
            // 'link:ntext',
            // 'linkattributes:ntext',
            // 'oldurl:ntext',
            // 'description:ntext',
            // 'fulltext:ntext',
             'date',
            [
                'attribute'=>'createuserid',

                'content'=>function($data){
                    return Yii::$app->getUserById($data->createuserid);
                }
            ],
            // 'sourcename',
            // 'sourceurl:ntext',
            // 'createdate',
            // 'isactive',
            // 'imagehorizontal',
            [
                'attribute'=>'edituserid',

                'content'=>function($data){

                    return ($data->edituserid == null)?$data->edituserid:Yii::$app->getUserById($data->edituserid);
                }
            ],

            'editdate',

            [
                'attribute'=>'newsviews',

                'content'=>function($data){

                    return !!$data->newsviews?$data->newsviews->count:0;
                }
            ],
            // 'isremoved',
            // 'url:ntext',
            // 'imagesourcename:ntext',
            // 'viewcount',
            // 'titlefordisplay',
            // 'updatedate',
            [
                'attribute'=>'nurl',

                'content'=>function($data){

                    return $data->url;
                }
            ],
            // 'nurl:url',
            // 'videotype',
            // 'fototype',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
