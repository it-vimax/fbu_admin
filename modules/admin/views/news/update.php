<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\News */

$this->title = 'Оновить новость: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->newsid]];
$this->params['breadcrumbs'][] = 'Оновить';
?>
<div class="news-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Данные',
                'content' => $this->render('_form', ['model' => $model, 'form' => $form]),
                'active' => true
            ],
            [
                'label' => 'Фотографии',
                'content' => $this->render('_form_images', ['newsimage' => $newsimage, 'form' => $form, 'newsid'=>$model->newsid]),
            ],
            [
                'label' => 'Теги',
                'content' => $this->render('_form_topics', ['newstopics' => $newstopics, 'form' => $form, 'newsid'=>$model->newsid]),
            ],
        ]]);
    ?>
    <?php ActiveForm::end(); ?>

</div>
