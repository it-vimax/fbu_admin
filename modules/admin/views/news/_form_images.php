<?php

use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;

?>

<div class="newsimage-form">
    <?php $count_img = count($newsimage); ?>
    <?= Html::button('Добавить картинку', ['value' => Url::to(['news/addimage', 'index'=>$count_img, 'newsid'=>$newsid, 'position'=>$count_img]), 'id' => 'add', 'class'=>'btn btn-primary', ]); ?>

    <?= Html::button('Добавить много картинок', ['value' => Url::to(['news/addimages', 'index'=>$count_img, 'newsid'=>$newsid, 'position'=>$count_img]), 'id' => 'addmany', 'class'=>'btn btn-primary', ]); ?>
<div class="row">

    <div class="col-sm-3 new hidden">
    </div>
    <?php if(is_array($newsimage)){ ?>
<?php foreach ($newsimage as $index => $image){ ?>
    <div class="col-sm-3">
        <div class="hidden">
                <?= $form->field($image, "[$index]newsimageid")->textInput() ?>
                <?= $form->field($image, "[$index]newsid")->textInput() ?>
                <?= $form->field($image, "[$index]position")->textInput() ?>
        </div>
        <?php echo $form->field($image, "[$index]filename")->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
            'aspectRatio' => (16/9), //set the aspect ratio
            'showPreview' => true, //false to hide the preview
            'showDeletePickedImageConfirm' => false, //on true show warning before detach image
            'news_id'=>$image->newsid.'/gallery',

        ]); ?>
        <?= $form->field($image, "[$index]position")->textInput() ?>
        <?= $form->field($image, "[$index]title")->textInput() ?>


        <?= $form->field($image, "[$index]sourcename")->textInput() ?>
        <?= Html::button('Удалить картинку', ['value' => Url::to(['news/deleteimage', 'newsimageid'=>$image->newsimageid]), 'id' => 'delete', 'class'=>'btn btn-primary', ]); ?>
    </div>
 <?php }  ?>
    <?php }  else { ?>
    <?php }?>

</div>


<div class="result"></div>

    <style>

        .image-manager-input {
            height: 150px;
            overflow: hidden;
        }
    </style>
<?php

$script = <<< JS

    jQuery('body').on('click', '#delete', function (e) {
        e.preventDefault();
        var el = jQuery(this);
         jQuery('.result').load(jQuery(this).attr('value'), function() {
         el.parents('.col-sm-3').remove();
         });
         
        
    });
    jQuery('body').on('click', '#deletesession', function (e) {
        var el = jQuery(this);
        el.parents('.col-sm-3').remove();
    });
    jQuery('body').on('click', '#addmany', function (e) {
                e.preventDefault();
                jQuery('.newsimage-form .col-sm-3.new').load(jQuery(this).attr('value'), function() {
               
                  jQuery(this).find('#one-image').unwrap(); //corigiren
                 jQuery(this).removeClass('new');
                 jQuery(this).removeClass('hidden');
                  jQuery('.newsimage-form .row').prepend('<div class="col-sm-3 new hidden"></div>');  
                  
                });
                
            });
    
    jQuery('body').on('click', '#add', function (e) {
                e.preventDefault();
                jQuery('.newsimage-form .col-sm-3.new').load(jQuery(this).attr('value'), function() {
               
                  jQuery(this).find('#one-image').unwrap();
                 jQuery(this).removeClass('new');
                 jQuery(this).removeClass('hidden');
                  jQuery('.newsimage-form .row').prepend('<div class="col-sm-3 new hidden"></div>');  
                  
                });
                
            });
    
JS;
$this->registerJs($script, View::POS_READY); ?>
</div>

