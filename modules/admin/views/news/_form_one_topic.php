<?php
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\admin\models\Topic;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
$topics = Topic::find()->where(['siteid'=>1])->orderBy(['topicid'=>SORT_ASC])->asArray()->all();
$topics = ArrayHelper::map($topics, 'topicid', 'name');

?>
<?php $form = ActiveForm::begin([]); ?>
<div id="one-topic">
    <div class="hidden">
        <?= $form->field($model, "[$index]newsid")->textInput() ?>
    </div>
    <?php echo $form->field($model, "[$index]topicid")->widget(Select2::classname(), [
    'data' => $topics,
    'language' => 'ru',
    'options' => ['placeholder' => 'Выберите топик ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>
    
    <?= $form->field($model, "[$index]sort_order")->textInput() ?>
<?= Html::button('Удалить тег', ['value' => Url::to(['news/deletetag']), 'id' => 'deletesessiontag', 'class'=>'btn btn-primary', ]); ?>
<?php ActiveForm::end(); ?>
<?php unset($form);?>
