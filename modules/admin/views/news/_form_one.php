<?php
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $form = ActiveForm::begin([]); ?>
    <div id="one-image" class="<?=$multi?'multi':''?>">

    <div class="hidden">
        <?= $form->field($model, "[$index]newsimageid")->textInput() ?>
        <?= $form->field($model, "[$index]newsid")->textInput() ?>
        <?= $form->field($model, "[$index]position")->textInput(['class' => 'position change']) ?>
    </div>
        <div class="check <?=$multi?'load':''?>">
                <?php echo $form->field($model, "[$index]filename")->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
                    'aspectRatio' => (16/9), //set the aspect ratio
                    'showPreview' => true, //false to hide the preview
                    'showDeletePickedImageConfirm' => false, //on true show warning before detach image
                    'news_id'=>$model->newsid.'/gallery',
                    'type_input'=>$multi?'multi':''
                ]); ?>

        </div>

    <?= $form->field($model, "[$index]title")->textInput() ?>

    <?= $form->field($model, "[$index]position")->textInput() ?>
    <?= $form->field($model, "[$index]sourcename")->textInput() ?></div>
    <?= Html::button('Удалить картинку', ['value' => Url::to(['news/deleteImage']), 'id' => 'deletesession', 'class'=>'btn btn-primary', ]); ?>
<?php ActiveForm::end(); ?>
<?php
if($multi) {
    $script = <<< JS
    
     var timerId;
    if($('#one-image .check').hasClass('load')){
        timerId = setInterval(loadmorefotos, 100);
    } else {
        clearInterval(timerId);
    }
    function loadmorefotos(){
    if($('#one-image .check').hasClass('load')){
     
        if($('#one-image.multi .check input[type="text"]').val() != ''){
        
        
        var images = $('#one-image.multi .check input[type="text"]').val();
        
        var array_images = images.split('/');
       $('.newsimage-form .col-sm-3.new').next().remove();
        
            var length_arr = array_images.length > 1?array_images.length - 1:0;
            for(var i = 0; i < length_arr; i++){
               var urladd = $('#add').attr('value');
               urladd += '&filename='+decodeURIComponent(array_images[i]);
                $.ajax({
                   url: urladd,
                     type: 'get',
                     dataType: 'html',
                     success: function(html) {
                         $('.newsimage-form .col-sm-3.new').html(html);
                          $('.newsimage-form .col-sm-3.new').find('#one-image').unwrap(); 
                          $('.newsimage-form .col-sm-3.new').removeClass('new').removeClass('hidden');
                           $('.newsimage-form .row').prepend('<div class="col-sm-3 new hidden"></div>');  
                     }
                })
              
                 
               
                
            
            }
             
            $('#one-image .check').removeClass('load');    
                
           
        }
    
    }
        
    
    }
    
    
JS;
    $this->registerJs($script, View::POS_READY);
}
?>

<?php unset($form);?>
