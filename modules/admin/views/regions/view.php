<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Regions */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Регионы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="regions-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(['id'=>'region_clubs_schools', 'action'=>'/admin/regions/addsc?id='.$model->region_id]); ?>

    <div class="form-group">
        <?= Html::submitButton('Обновить всю информацию', ['class' => 'btn btn-primary']) ?>
    </div>
    <?= Tabs::widget([
        'items' => [
            [
                'label' => 'Клубы',
                'content' => $this->render('_formclub', ['clubs' => $clubs, 'form'=>$form, 'region_id'=>$model->region_id]),
                'active' => true
            ],

            [
                'label' => 'Школы',
                'content' => $this->render('_formschool', ['schools' => $schools, 'form'=>$form, 'region_id'=>$model->region_id]),
                'active' => false
            ],
            [
                'label' => 'Руководство',
                'content' => $this->render('_formleaders', ['leaders' => $leaders, 'form'=>$form, 'region_id'=>$model->region_id]),
                'active' => false
            ],

        ]]);
    ?>

    <?php ActiveForm::end(); ?>

</div>
<style>
    .school-form, .newsimage-form, .leaders-form {
        margin: 20px 0;
    }
</style>