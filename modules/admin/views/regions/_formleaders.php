<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use dosamigos\ckeditor\CKEditor;
?>

<div class="leaders-form">
    <?php $count_leaders = count($leaders); ?>
    <?= Html::button('Добавить руководителя', ['value' => Url::to(['regions/addleader', 'index'=>$count_leaders, 'region_id'=>$region_id]), 'id' => 'addleader', 'class'=>'btn btn-primary', ]); ?>
    <div class="row">
        <div class="col-sm-4 new hidden">
        </div>
        <?php if(is_array($leaders)){ ?>
            <?php foreach ($leaders as $index => $leader){ ?>
                <div class="col-sm-4">
                    <div class="hidden">
                        <?= $form->field($leader, "[$index]region_id")->textInput() ?>
                        <?= $form->field($leader, "[$index]created_user")->textInput() ?>

                    </div>


                    <?= $form->field($leader, "[$index]fio")->textInput() ?>
                    <?= $form->field($leader, "[$index]position")->textInput() ?>
                    <?php echo $form->field($leader, "[$index]foto")->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
                        'aspectRatio' => (16/9), //set the aspect ratio
                        'showPreview' => true, //false to hide the preview
                        'showDeletePickedImageConfirm' => false, //on true show warning before detach image
                        'news_id'=>'gameswidget',
                    ]); ?>
                    

                    <?= $form->field($leader, "[$index]sort_order")->textInput() ?>
                        

                    <?= Html::button('Удалить руководителя', ['value' => Url::to(['regions/deleteleader', 'region_leader_id'=>$leader->id]), 'id' => 'deleteleader', 'class'=>'btn btn-primary', ]); ?>
                </div>
            <?php }  ?>
        <?php }  else { ?>
        <?php }?>
    </div>
    <div class="result"></div>
    <?php
    $script = <<< JS

    jQuery('body').on('click', '#deleteleader', function (e) {
    e.preventDefault();
    var el = jQuery(this);
    jQuery('.result').load(jQuery(this).attr('value'), function() {
    el.parents('.col-sm-4').remove();
    });


    });
    jQuery('body').on('click', '#deletesessionleader', function (e) {
    var el = jQuery(this);
    el.parents('.col-sm-4').remove();
    })

    jQuery('body').on('click', '#addleader', function (e) {
    e.preventDefault();
    jQuery('.leaders-form .col-sm-4.new').load(jQuery(this).attr('value'), function() {

    jQuery(this).find('#one-leader').unwrap();
    jQuery(this).removeClass('new');
    jQuery(this).removeClass('hidden');
    jQuery('.leaders-form .row').prepend('<div class="col-sm-4 new hidden"></div>');

    });

    });
JS;
    $this->registerJs($script, View::POS_READY); ?>
</div>