<?php
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use dosamigos\ckeditor\CKEditor;
?>
<?php $form = ActiveForm::begin([]); ?>
<div id="one-club">



    <div class="hidden">
        <?= $form->field($model, "[$index]region_id")->textInput() ?>
        <?= $form->field($model, "[$index]created_user")->textInput() ?>

    </div>


    <?= $form->field($model, "[$index]team_name")->textInput() ?>
    <?php echo $form->field($model, "[$index]logo")->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
        'aspectRatio' => (16/9), //set the aspect ratio
        'showPreview' => true, //false to hide the preview
        'showDeletePickedImageConfirm' => false, //on true show warning before detach image
        'news_id'=>'gameswidget',
    ]); ?>

    <?= $form->field($model, "[$index]link")->textInput() ?>
    <?= $form->field($model, "[$index]site_club")->textInput() ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news_id'=>'gameswidget']),
        ]
    ]) ?>
</div>
<?= Html::button('Удалить клуб', ['value' => Url::to(['news/deleteclub']), 'id' => 'deletesession', 'class'=>'btn btn-primary', ]); ?>
<?php ActiveForm::end(); ?>
<?php unset($form);?>
