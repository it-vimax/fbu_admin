<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use dosamigos\ckeditor\CKEditor;
?>

<div class="school-form">
    <?php $count_school = count($schools); ?>
    <?= Html::button('Добавить школу', ['value' => Url::to(['regions/addschool', 'index'=>$count_school, 'region_id'=>$region_id]), 'id' => 'addschool', 'class'=>'btn btn-primary', ]); ?>
    <div class="row">
        <div class="col-sm-4 new hidden">
        </div>
        <?php if(is_array($schools)){ ?>
            <?php foreach ($schools as $index => $school){ ?>
                <div class="col-sm-4">
                    <div class="hidden">
                        <?= $form->field($school, "[$index]region_id")->textInput() ?>
                        <?= $form->field($school, "[$index]created_user")->textInput() ?>

                    </div>


                    <?= $form->field($school, "[$index]name")->textInput() ?>
                    <?= $form->field($school, "[$index]separet_page")->radioList([0=>'Нет', 1=>'Да']) ?>
                    <?= $form->field($school, "[$index]contact")->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'preset' => 'advanced',
                        'clientOptions' => [
                            'filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news_id'=>'gameswidget']),
                        ]
                    ]) ?>
                    <?= $form->field($school, "[$index]description")->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'preset' => 'advanced',
                        'clientOptions' => [
                            'filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news_id'=>'gameswidget']),
                        ]
                    ]) ?>

                    <?= Html::button('Удалить школу', ['value' => Url::to(['regions/deleteschool', 'region_club_id'=>$school->id]), 'id' => 'deleteschool', 'class'=>'btn btn-primary', ]); ?>
                </div>
            <?php }  ?>
        <?php }  else { ?>
        <?php }?>
    </div>
    <div class="result"></div>
    <?php
    $script = <<< JS

    jQuery('body').on('click', '#deleteschool', function (e) {
    e.preventDefault();
    var el = jQuery(this);
    jQuery('.result').load(jQuery(this).attr('value'), function() {
    el.parents('.col-sm-4').remove();
    });


    });
    jQuery('body').on('click', '#deletesessionschool', function (e) {
    var el = jQuery(this);
    el.parents('.col-sm-4').remove();
    })

    jQuery('body').on('click', '#addschool', function (e) {
    e.preventDefault();
    jQuery('.school-form .col-sm-4.new').load(jQuery(this).attr('value'), function() {

    jQuery(this).find('#one-school').unwrap();
    jQuery(this).removeClass('new');
    jQuery(this).removeClass('hidden');
    jQuery('.school-form .row').prepend('<div class="col-sm-4 new hidden"></div>');

    });

    });
JS;
    $this->registerJs($script, View::POS_READY); ?>
</div>