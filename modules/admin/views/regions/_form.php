<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\models\Topic;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
$topics = Topic::find()->where(['siteid'=>1])->orderBy(['topicid'=>SORT_ASC])->asArray()->all();
$topics = ArrayHelper::map($topics, 'topicid', 'name');
$region_list = array(
  0=>'Вінницька область',
  1=>'Волинська область',
    2=>'Дніпропетровська область',
    3=>'Донецька область',
    4=>'Житомирська область',
    5=>'Закарпатська область',
    6=>'Запорізька область',
    7=>'Івано-Франківська область',
    8=>'Київська область',
    9=>'Кіровоградська область',
    10=>'Луганська область',
    11=>'Львівська область',
    12=>'Миколаївська область',
    13=>'Одеська область',
    14=>'Полтавська область',
    15=>'Рівненська область',
    16=>'Сумська область',
    17=>'Тернопільська область',
    18=>'Харківська область',
    19=>'Херсонська область ',
    20=>'Хмельницька область',
    21=>'Черкаська область',
    22=>'Чернівецька область',
    23=>'Чернігівська область',
    24=>'м. Київ',
    25=>'м. Севастополь',
    26 =>'Автономна республіка Крим'
);
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Regions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="regions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'region_id')->dropDownList($region_list) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'site_link')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'topicid')->dropDownList($topics) ?>

    <?= $form->field($model, 'amatour_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contacts')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news-id'=> 'gameswidget']),
        ]
    ]) ?>
    <div class="hidden">
    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_user')->textInput() ?>

    <?= $form->field($model, 'date_added')->textInput() ?>

    <?= $form->field($model, 'modificated_user')->textInput() ?>

    <?= $form->field($model, 'date_modificated')->textInput() ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
