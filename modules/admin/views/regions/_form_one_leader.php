<?php
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use dosamigos\ckeditor\CKEditor;
?>
<?php $form = ActiveForm::begin([]); ?>
<div id="one-leader">



    <div class="hidden">
                        <?= $form->field($model, "[$index]region_id")->textInput() ?>
                        <?= $form->field($model, "[$index]created_user")->textInput() ?>

                    </div>


                    <?= $form->field($model, "[$index]fio")->textInput() ?>
                    <?= $form->field($model, "[$index]position")->textInput() ?>
                    <?php echo $form->field($model, "[$index]foto")->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
                        'aspectRatio' => (16/9), //set the aspect ratio
                        'showPreview' => true, //false to hide the preview
                        'showDeletePickedImageConfirm' => false, //on true show warning before detach image
                        'news_id'=>'gameswidget',
                    ]); ?>

                    <?= $form->field($model, "[$index]sort_order")->textInput(['value'=>$index]) ?>
</div>
<?= Html::button('Удалить руководителя', ['value' => Url::to(['news/deleteleader']), 'id' => 'deletesessionleader', 'class'=>'btn btn-primary', ]); ?>
<?php ActiveForm::end(); ?>
<?php unset($form);?>
