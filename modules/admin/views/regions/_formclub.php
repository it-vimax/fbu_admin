<?php
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use dosamigos\ckeditor\CKEditor;
?>

<div class="newsimage-form">
    <?php $count_club = count($clubs); ?>
    <?= Html::button('Добавить клуб', ['value' => Url::to(['regions/addclub', 'index'=>$count_club, 'region_id'=>$region_id]), 'id' => 'add', 'class'=>'btn btn-primary', ]); ?>
    <div class="row">
        <div class="col-sm-4 new hidden">
        </div>
<?php if(is_array($clubs)){ ?>
    <?php foreach ($clubs as $index => $club){ ?>
        <div class="col-sm-4">
            <div class="hidden">
                <?= $form->field($club, "[$index]region_id")->textInput() ?>
                <?= $form->field($club, "[$index]created_user")->textInput() ?>

            </div>


            <?= $form->field($club, "[$index]team_name")->textInput() ?>
            <?php echo $form->field($club, "[$index]logo")->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
                'aspectRatio' => (16/9), //set the aspect ratio
                'showPreview' => true, //false to hide the preview
                'showDeletePickedImageConfirm' => false, //on true show warning before detach image
                'news_id'=>'gameswidget',
            ]); ?>

            <?= $form->field($club, "[$index]link")->textInput() ?>
            <?= $form->field($club, "[$index]site_club")->textInput() ?>

            <?= $form->field($club, 'description')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'advanced',
                'clientOptions' => [
                    'filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news_id'=>'gameswidget']),
                ]
            ]) ?>

            <?= Html::button('Удалить клуб', ['value' => Url::to(['regions/deleteclub', 'region_club_id'=>$club->id]), 'id' => 'delete', 'class'=>'btn btn-primary', ]); ?>
        </div>
    <?php }  ?>
<?php }  else { ?>
<?php }?>
</div>
<div class="result"></div>
<?php
$script = <<< JS

    jQuery('body').on('click', '#delete', function (e) {
    e.preventDefault();
    var el = jQuery(this);
    jQuery('.result').load(jQuery(this).attr('value'), function() {
    el.parents('.col-sm-4').remove();
    });


    });
    jQuery('body').on('click', '#deletesession', function (e) {
    var el = jQuery(this);
    el.parents('.col-sm-4').remove();
    })

    jQuery('body').on('click', '#add', function (e) {
    e.preventDefault();
    jQuery('.newsimage-form .col-sm-4.new').load(jQuery(this).attr('value'), function() {

    jQuery(this).find('#one-club').unwrap();
    jQuery(this).removeClass('new');
    jQuery(this).removeClass('hidden');
    jQuery('.newsimage-form .row').prepend('<div class="col-sm-4 new hidden"></div>');

    });

    });
JS;
    $this->registerJs($script, View::POS_READY); ?>
</div>