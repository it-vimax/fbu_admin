<?php
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use dosamigos\ckeditor\CKEditor;
?>
<?php $form = ActiveForm::begin([]); ?>
<div id="one-school">



    <div class="hidden">
        <?= $form->field($model, "[$index]region_id")->textInput() ?>
        <?= $form->field($model, "[$index]created_user")->textInput() ?>

    </div>




    <?= $form->field($model, "[$index]name")->textInput() ?>
    <?= $form->field($model, "[$index]separet_page")->radioList([0=>'Нет', 1=>'Да']) ?>
        <?= $form->field($model, "[$index]contact")->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news_id'=>'gameswidget']),
        ]
    ]) ?>
    <?= $form->field($model, "[$index]description")->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advanced',
        'clientOptions' => [
            'filebrowserImageBrowseUrl' => yii\helpers\Url::to(['/imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'ckeditor', 'news_id'=>'gameswidget']),
        ]
    ]) ?>
</div>
<?= Html::button('Удалить школу', ['value' => Url::to(['news/deleteschool']), 'id' => 'deletesessionschool', 'class'=>'btn btn-primary', ]); ?>
<?php ActiveForm::end(); ?>
<?php unset($form);?>
