<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datetimepicker\DateTimePicker;
$league_array = array(
    '0'=>'Не указано',
  'league-224'=>'Суперліга Парі-Матч',
    'league-32079'=>'Суперліга (жінки)',
    'league-8185'=>'Вища лiга',
    'league-10843'=>'Вища лiга (жінки)',
    'league-11343'=>'Перша ліга',
    'league-8381'=>'Кубок України (чоловіки)',
    'league-31503'=>'Кубок України (жінки)',
    'league-31497'=>'Студентська ліга чоловіки',
    'league-31833'=>'Студентська ліга жінки',
    'league-27851'=>'Юнацька ліга',
    'team-491'=>'Національна чоловіча збірна',
    'team-591'=>'Національна жіноча збірна',
    'team-621'=>'Молодша чоловіча збірна (U20)',
    'team-611'=>'Молодша жіноча збірна (U20)',
    'team-631'=>'Юніорська чоловіча збірна (U18)',
    'team-601'=>'Юніорська жіноча збірна (U18)',
    'team-651'=>'Кадетська чоловіча збірна (U16)',
    'team-641'=>'Кадетська жіноча збірна (U16)',
);
/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Gameswidget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gameswidget-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-12 text-center"><h2>Информация о командах</h2></div>
        <div class="col-sm-6">
            <div class="h3 text-center">Команда 1</div>
            <?= $form->field($model, 'teama_name')->textInput(['maxlength' => true]) ?>
            <?php echo $form->field($model, 'teama_image')->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
                'aspectRatio' => (16/9), //set the aspect ratio
                'showPreview' => true, //false to hide the preview
                'showDeletePickedImageConfirm' => false, //on true show warning before detach image
                'news_id'=>'gameswidget',
            ]); ?>


            <?= $form->field($model, 'teama_score')->textInput() ?>
            <?= $form->field($model, 'teama_link')->textInput() ?>
        </div>
        <div class="col-sm-6">
            <div class="h3 text-center">Команда 2</div>
            <?= $form->field($model, 'teamb_name')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'teamb_image')->widget(\noam148\imagemanager\components\ImageManagerInputWidget::className(), [
                'aspectRatio' => (16/9), //set the aspect ratio
                'showPreview' => true, //false to hide the preview
                'showDeletePickedImageConfirm' => false, //on true show warning before detach image
                'news_id'=>'gameswidget',
            ]); ?>

            <?= $form->field($model, 'teamb_score')->textInput() ?>
            <?= $form->field($model, 'teamb_link')->textInput() ?>
        </div>
    </div>
    <div class="row">

        <div class="col-sm-3">
            <div class="col-sm-12 text-center"><h2>Детали игры</h2></div>
            <?= $form->field($model, 'ranggame')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'date_game')->widget(DateTimePicker::className(), [
                    'language' => 'ru',
                    'size' => 'ms',
                    'template' => '{input}',
                    'pickButtonIcon' => 'glyphicon glyphicon-time',
                    'inline' => false,
                    'clientOptions' => [
                        'startView' => 2,
                        'minView' => 0,
                        'maxView' => 3,
                        'autoclose' => true,
                        //'linkFormat' => 'HH:ii P', // if inline = true
                        'format' => 'yyyy-mm-dd HH:ii:ss', // if inline = false
                        'todayBtn' => true
                    ]
                ]);?>
                <?= $form->field($model, 'endgametime')->widget(DateTimePicker::className(), [
                    'language' => 'ru',
                    'size' => 'ms',
                    'template' => '{input}',
                    'pickButtonIcon' => 'glyphicon glyphicon-time',
                    'inline' => false,
                    'clientOptions' => [
                        'startView' => 2,
                        'minView' => 0,
                        'maxView' => 3,
                        'autoclose' => true,
                        //'linkFormat' => 'HH:ii P', // if inline = true
                        'format' => 'yyyy-mm-dd HH:ii:ss', // if inline = false
                        'todayBtn' => true
                    ]
                ]);?>
            <?= $form->field($model, 'place_game')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'league_type')->dropDownList($league_array) ?>
            <?= $form->field($model, 'tourn')->textInput() ?>

            <?= $form->field($model, 'active')->radioList([1 => 'Да', 0 => 'Нет']) ?>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12 text-center"><h2>Статистика игры</h2></div>
            <?= $form->field($model, 'foto_link')->textInput() ?>
            <?= $form->field($model, 'video_link')->textInput() ?>
            <?= $form->field($model, 'stats_link')->textInput() ?>
            <?= $form->field($model, 'post_link')->textInput() ?>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-12 text-center"><h2>Трансляции</h2></div>
            <?= $form->field($model, 'youtube_link')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'xssport_link')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="hidden">
        <?= $form->field($model, 'createruserid')->textInput() ?>
        <?= $form->field($model, 'edituserid')->textInput() ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<style>

    .image-manager-input {
        height: 150px;
        overflow: hidden;
    }
</style>