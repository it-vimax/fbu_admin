<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\GameswidgetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gameswidget-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'teama_name') ?>

    <?= $form->field($model, 'teama_image') ?>

    <?= $form->field($model, 'teama_score') ?>

    <?= $form->field($model, 'teamb_name') ?>

    <?php // echo $form->field($model, 'teamb_image') ?>

    <?php // echo $form->field($model, 'teamb_score') ?>

    <?php // echo $form->field($model, 'ranggame') ?>

    <?php // echo $form->field($model, 'date_game') ?>

    <?php // echo $form->field($model, 'time_game') ?>

    <?php // echo $form->field($model, 'place_game') ?>

    <?php // echo $form->field($model, 'foto_link') ?>

    <?php // echo $form->field($model, 'video_link') ?>

    <?php // echo $form->field($model, 'stats_link') ?>

    <?php // echo $form->field($model, 'post_link') ?>

    <?php // echo $form->field($model, 'youtube_link') ?>

    <?php // echo $form->field($model, 'xssport_link') ?>

    <?php // echo $form->field($model, 'createruserid') ?>

    <?php // echo $form->field($model, 'edituserid') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
