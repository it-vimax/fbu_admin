<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Gameswidget */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Gameswidgets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gameswidget-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'teama_name',
            'teama_image',
            'teama_score',
            'teamb_name',
            'teamb_image',
            'teamb_score',
            'ranggame',
            'date_game',

            'place_game',
            'foto_link:ntext',
            'video_link:ntext',
            'stats_link:ntext',
            'post_link:ntext',
            'youtube_link',
            'xssport_link',
            'createruserid',
            'edituserid',
        ],
    ]) ?>

</div>
