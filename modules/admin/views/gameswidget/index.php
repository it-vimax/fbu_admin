<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\GameswidgetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Игры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gameswidget-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить игру', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'teama_name',
            //'teama_image',
           // 'teama_score',
            'teamb_name',
            // 'teamb_image',
            // 'teamb_score',
            // 'ranggame',
             'date_game',
            // 'time_game',
            // 'place_game',
            // 'foto_link:ntext',
            // 'video_link:ntext',
            // 'stats_link:ntext',
            // 'post_link:ntext',
            // 'youtube_link',
            // 'xssport_link',
            // 'createruserid',
            // 'edituserid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
