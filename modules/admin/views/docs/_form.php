<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\ElFinder;
use yii\web\JsExpression;
use app\modules\admin\models\Doccategories;

$doccats = Doccategories::find()->orderBy(['doccatid'=>SORT_ASC])->asArray()->all();
$docsarray = array();
foreach($doccats as $doccat){
    $docsarray[$doccat['parent_id']][] = array(
            'name'=>$doccat['name'],             
            'parent_id' => $doccat['parent_id'],
            'doccatid' => $doccat['doccatid']
          );

}


function recurse_array($array, &$chandbarray, $start = 0, $preffix = ''){
    
    
    foreach($array as $index =>$arr){
            if($start == $index){
            
                    foreach($arr as $small_arr){

                            $chandbarray[$small_arr['doccatid']] = $preffix.$small_arr['name'];
                            if(isset($array[$small_arr['doccatid']])){
                                $preffix_dot = $preffix .'--';
                                    
                                    recurse_array($array, $chandbarray, $small_arr['doccatid'], $preffix_dot);

                                    
                            }
                    } 
            }
    }
    
}

$doccats = array();
recurse_array($docsarray, $result_array);

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Docs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="docs-form">
<div class="col-sm-6">  
    <?php $form = ActiveForm::begin(); ?>

     <?= $form->field($model, 'catid')->dropDownList($result_array) ?>

    <?= $form->field($model, 'docname')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'doclink')->widget(InputFile::className(), [
    'language'      => 'ru',
    'controller'    => 'elfinder', // вставляем название контроллера, по умолчанию равен elfinder
    'path' => '', // будет открыта папка из настроек контроллера с добавлением указанной под деритории 
    //'filter'        => 'image',    // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
    'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
    'options'       => ['class' => 'form-control'],
    'buttonOptions' => ['class' => 'btn btn-default'],
    'multiple'      => false       // возможность выбора нескольких файлов
]); ?>

    

   

    <?= $form->field($model, 'sort_order')->textInput() ?>

   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
