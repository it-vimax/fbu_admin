<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Doccategories;

$doccats = Doccategories::find()->orderBy(['doccatid'=>SORT_ASC])->asArray()->all();
$docsarray = array();
foreach($doccats as $doccat){
	$docsarray[$doccat['parent_id']][] = array(
		    'name'=>$doccat['name'],		     
		    'parent_id' => $doccat['parent_id'],
		    'doccatid' => $doccat['doccatid']
		  );

}

$result_array[0] = "Главная";
function recurse_array($array, &$chandbarray, $start = 0, $preffix = ''){
	
	
	foreach($array as $index =>$arr){
			if($start == $index){
			
					foreach($arr as $small_arr){

							$chandbarray[$small_arr['doccatid']] = $preffix.$small_arr['name'];
							if(isset($array[$small_arr['doccatid']])){
								$preffix_dot = $preffix .'--';
									
									recurse_array($array, $chandbarray, $small_arr['doccatid'], $preffix_dot);

									
							}
					} 
			}
	}
	
}

$doccats = array();
recurse_array($docsarray, $result_array);

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Doccategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="doccategories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->dropDownList($result_array) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort_order')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
