<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\News;
use app\modules\admin\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\models\Newsimage;
use  app\modules\admin\models\Newstopics;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
       Yii::$app->cache->flush();
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSave()
    {
        $session = Yii::$app->session;
        $session->remove('index-image');
        $model = new News();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $fulltext = $model->fulltext;
            $fulltext = str_replace("http://https://", "https://", $fulltext);
            $fulltext = str_replace("https://http://", "http://", $fulltext);
            $fulltext = str_replace("http://http://", "http://", $fulltext);
            $fulltext = str_replace("https://https://", "https://", $fulltext);
            $model->fulltext = $fulltext;
            $model->save();


            if (!empty(Yii::$app->request->post())) {
                $newsimages_arr = isset(Yii::$app->request->post()['Newsimage']) ? Yii::$app->request->post()['Newsimage'] : false;
                if ($newsimages_arr) {
                    foreach ($newsimages_arr as $newsimg) {
                        $img['Newsimage'] = $newsimg;
                        $newsimage_block = new Newsimage();
                        $newsimage_block->load($img);
                        $newsimage_block->save();
                    }
                }

                $newstopics_arr = isset(Yii::$app->request->post()['Newstopics']) ? Yii::$app->request->post()['Newstopics'] : false;
                if ($newstopics_arr) {
                    foreach ($newstopics_arr as $newstop) {
                        $top['Newstopics'] = $newstop;
                        $newstop_block = new Newstopics();
                        $newstop_block->load($top);
                        $newstop_block->save();
                    }
                }
            }

        }
        return $this->redirect(['index']);
    }
    public function actionCreate()
    {
        $session = Yii::$app->session;
        $session->remove('index-image');
        $model = new News();
        $newsimage = new Newsimage();
        $newstopics = new Newstopics();

            $last_created_id = Yii::$app->cache->get('last_id');
            if ($last_created_id === false) {
                $news_id = News::find(['newsid'])->orderBy(['newsid' => SORT_DESC])->one();
                $last_created_id = $news_id->newsid + 1;

                Yii::$app->cache->set('last_id', $last_created_id);

            } else {
                $news_id = $last_created_id + 1;
                Yii::$app->cache->set('last_id', $news_id);
            }

        $news_id = $last_created_id;
        $newsimage->newsid =$news_id;
        $newstopics->newsid = $news_id;
        $newsimage->position =0;
        $model->newsid = $news_id;
        $model->createuserid = Yii::$app->getUser()->id;
        $model->edituserid = Yii::$app->getUser()->id;
        $model->viewcount = 0;
        $model->isremoved = 0;
        $model->isactive = 1;
        $model->videotype = 0;
        $model->fototype = 0;
        $model->createdate = date("Y-m-d H:i:s");
        $model->date = date("Y-m-d H:i:s");
        $model->editdate = date("Y-m-d H:i:s");
        $model->updatedate = date("Y-m-d H:i:s");


           return $this->render('create', [
                'model' => $model,
                'newsimage'=>$newsimage,
                'newstopics'=>$newstopics

            ]);

    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $session = Yii::$app->session;
        $session->remove('index-image');
        $session->remove('index-topic');
        $model = $this->findModel($id);
        $model->edituserid = Yii::$app->getUser()->id;
        $model->editdate = date("Y-m-d H:i:s");
        $newsimage = Newsimage::find()->where(['newsid'=>$id])->orderBy(['position'=>SORT_DESC])->all();
        $newstopics = Newstopics::find()->where(['newsid'=>$id])->orderBy(['sort_order'=>SORT_DESC])->all();
        if(!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['Newstopics'])){
            $newstopic_arr = Yii::$app->request->post()['Newstopics'];
            Newstopics::deleteAll(['newsid' => $id]);
            foreach ($newstopic_arr as $newstop){
                $top['Newstopics']=$newstop;
                $newstop_block = new Newstopics();
                $newstop_block->load($top);
                $newstop_block->save();
            }
        }
       if(!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['Newsimage'])){
            $newsimages_arr = Yii::$app->request->post()['Newsimage'];
            foreach ($newsimages_arr as $newsimg){
                if(!$newsimg['newsimageid']){
                    $img['Newsimage']=$newsimg;
                    $newsimage_block = new Newsimage();
                   $newsimage_block->load($img);
                   $newsimage_block->save();
                }
            }
        }
        if(isset(Yii::$app->request->post()['Newsimage'])) {
            if (Newsimage::loadMultiple($newsimage, Yii::$app->request->post()) && Newsimage::validateMultiple($newsimage)) {
                foreach ($newsimage as $image) {
                    $image->save(false);
                }
            }
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $fulltext = $model->fulltext;
            $fulltext = str_replace("http://https://", "https://", $fulltext);
            $fulltext = str_replace("https://http://", "http://", $fulltext);
            $fulltext = str_replace("http://http://", "http://", $fulltext);
            $fulltext = str_replace("https://https://", "https://", $fulltext);
            $model->fulltext = $fulltext;
            $model->save();

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'newsimage'=>$newsimage,
                'newstopics'=>$newstopics
            ]);
        }
    }

    public function actionDeleteimage($newsimageid){
        $model = Newsimage::find()->where(['newsimageid'=>$newsimageid])->one();
        $model->delete();
        return $this->renderAjax('_delete');

    }
    public function actionDeletetag($topicid, $newsid){
        $model = Newstopics::find()->where(['newsid'=>$newsid, 'topicid'=>$topicid])->one();
        $model->delete();
        return $this->renderAjax('_delete');

    }
    public function actionAddtag($index, $newsid){
        $model= new Newstopics();
        $model->newsid= $newsid;
        $session = Yii::$app->session;
        if(!$session->isActive){
            $session->open();
        }
        $index_cache = $session->get('index-topic');
        if(!$index_cache){
            $index_cache = $index + 1;
            $session->set('index-topic', $index_cache);
        } else {
            $index_cache = $index_cache + 1;
            $session->set('index-topic', $index_cache);
        }
        $model->sort_order= $index_cache;
        return $this->renderAjax('_form_one_topic', [
            'model' => $model,
            'index'=>$index_cache
        ]);
    }
    public function actionAddimage($index, $newsid, $position, $filename = ''){

        $model= new Newsimage();

        $model->newsid= $newsid;
        $model->filename = urldecode($filename);
        $session = Yii::$app->session;
        if(!$session->isActive){
            $session->open();
        }
        $index_cache = $session->get('index-image');
        if(!$index_cache){
            $index_cache = $index + 1;
            $session->set('index-image', $index_cache);
        } else {
            $index_cache = $index_cache + 1;
            $session->set('index-image', $index_cache);
        }
        $model->position= $index_cache;
        return $this->renderAjax('_form_one', [
            'model' => $model,
            'index'=>$index_cache,
            'multi'=>false
        ]);
    }


    public function actionAddimages($index, $newsid, $position){

        $model= new Newsimage();

        $model->newsid= $newsid;
        $session = Yii::$app->session;
        if(!$session->isActive){
            $session->open();
        }
        $index_cache = $session->get('index-image');
        if(!$index_cache){
            $index_cache = $index + 1;
            $session->set('index-image', $index_cache);
        } else {
            $index_cache = $index_cache + 1;
            $session->set('index-image', $index_cache);
        }
        $model->position= $index_cache;
        return $this->renderAjax('_form_one', [
            'model' => $model,
            'index'=>$index_cache,
            'multi'=>true
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model =  $this->findModel($id);
        $model->isremoved = 1;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
