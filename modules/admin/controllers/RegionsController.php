<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Regions;
use app\modules\admin\models\RegionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\models\RegionClubs;
use app\modules\admin\models\RegionSchool;
use app\modules\admin\models\RegionLeaders;
/**
 * RegionsController implements the CRUD actions for Regions model.
 */
class RegionsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Regions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RegionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Regions model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $clubs = RegionClubs::find()->where(['region_id'=>$id])->all();
        $schools = RegionSchool::find()->where(['region_id'=>$id])->all();
        $leaders = RegionLeaders::find()->where(['region_id'=>$id])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'clubs'=>$clubs,
            'schools'=>$schools,
            'leaders'=>$leaders,
        ]);
    }

    /**
     * Creates a new Regions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Regions();
        $model->created_user = Yii::$app->getUser()->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->region_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    public function actionAddsc($id)
    {

        if(!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['RegionClubs'])){
            $newstopic_arr = Yii::$app->request->post()['RegionClubs'];
            RegionClubs::deleteAll(['region_id' => $id]);
            foreach ($newstopic_arr as $newstop){
                $top['RegionClubs']=$newstop;
                $newstop_block = new RegionClubs();
                $newstop_block->load($top);

                $newstop_block->save();
            }
        }

        if(!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['RegionSchool'])){
            $newstopic_arr = Yii::$app->request->post()['RegionSchool'];
            RegionSchool::deleteAll(['region_id' => $id]);
            foreach ($newstopic_arr as $newstop){
                $top['RegionSchool']=$newstop;
                $newstop_block = new RegionSchool();
                $newstop_block->load($top);
                $newstop_block->save();
            }
        }

        if(!empty(Yii::$app->request->post()) && isset(Yii::$app->request->post()['RegionLeaders'])){
            $newstopic_arr = Yii::$app->request->post()['RegionLeaders'];
            RegionLeaders::deleteAll(['region_id' => $id]);
            foreach ($newstopic_arr as $newstop){
                $top['RegionLeaders']=$newstop;
                $newstop_block = new RegionLeaders();
                $newstop_block->load($top);
                $newstop_block->save();
            }
        }

        return $this->redirect(['view', 'id'=>$id]);
    }

    public function actionAddclub($index, $region_id){

        $model= new RegionClubs();

        $model->region_id= $region_id;
        $model->created_user = Yii::$app->getUser()->id;
        $session = Yii::$app->session;
        if(!$session->isActive){
            $session->open();
        }
        $index_cache = $session->get('index-club');
        if(!$index_cache){
            $index_cache = $index + 1;
            $session->set('index-club', $index_cache);
        } else {
            $index_cache = $index_cache + 1;
            $session->set('index-club', $index_cache);
        }

        return $this->renderAjax('_form_one_club', [
            'model' => $model,
            'index'=>$index_cache
        ]);
    }

    public function actionAddleader($index, $region_id){

        $model= new RegionLeaders();

        $model->region_id= $region_id;
        $model->created_user = Yii::$app->getUser()->id;
        $session = Yii::$app->session;
        if(!$session->isActive){
            $session->open();
        }
        $index_cache = $session->get('index-leader');
        if(!$index_cache){
            $index_cache = $index + 1;
            $session->set('index-leader', $index_cache);
        } else {
            $index_cache = $index_cache + 1;
            $session->set('index-leader', $index_cache);
        }

        return $this->renderAjax('_form_one_leader', [
            'model' => $model,
            'index'=>$index_cache
        ]);
    }
    public function actionDeleteleader($region_leader_id){
        $model = RegionLeaders::find()->where(['id'=>$region_leader_id])->one();
        $model->delete();
        return $this->renderAjax('_delete');

    }

    public function actionDeleteclub($region_club_id){
        $model = RegionClubs::find()->where(['id'=>$region_club_id])->one();
        $model->delete();
        return $this->renderAjax('_delete');

    }

    public function actionAddschool($index, $region_id){

        $model= new RegionSchool();

        $model->region_id= $region_id;
        $model->separet_page = 0;
        $model->created_user = Yii::$app->getUser()->id;
        $session = Yii::$app->session;
        if(!$session->isActive){
            $session->open();
        }
        $index_cache = $session->get('index-school');
        if(!$index_cache){
            $index_cache = $index + 1;
            $session->set('index-school', $index_cache);
        } else {
            $index_cache = $index_cache + 1;
            $session->set('index-school', $index_cache);
        }

        return $this->renderAjax('_form_one_school', [
            'model' => $model,
            'index'=>$index_cache
        ]);
    }


    public function actionDeleteschool($region_club_id){
        $model = RegionSchool::find()->where(['id'=>$region_club_id])->one();
        $model->delete();
        return $this->renderAjax('_delete');

    }

    /**
     * Updates an existing Regions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->modificated_user = Yii::$app->getUser()->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->region_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Regions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Regions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Regions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Regions::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
