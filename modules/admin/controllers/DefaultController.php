<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect('/admin/news/index');
    }
    public function actionClear()
    {
        $this->redirect('http://fbu.ua/clear');
        return $this->render('index');
    }
}
