<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "doccategories".
 *
 * @property integer $doccatid
 * @property integer $parent_id
 * @property string $name
 * @property integer $sort_order
 */
class Doccategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doccategories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort_order'], 'integer'],
            [['name', 'sort_order'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'doccatid' => 'Doccatid',
            'parent_id' => 'Родительськая категория',
            'name' => 'Название',
            'sort_order' => 'Порядок',
        ];
    }
}
