<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "questions".
 *
 * @property integer $id
 * @property string $question
 * @property string $fotos
 * @property string $videolink
 * @property string $date_quest
 * @property string $answer
 * @property integer $user_answer_id
 * @property string $date_answer
 * @property string $answer_title
 * @property integer $publish
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions';
    }
     public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_quest'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['date_answer'],
                ],
                'value' => date("Y-m-d H:i:s"),
            ],

        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question'], 'required'],
            [['question', 'fotos', 'answer'], 'string'],
            [['date_quest', 'date_answer'], 'safe'],
            [['user_answer_id', 'publish', 'page'], 'integer'],
            [['videolink', 'answer_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Вопрос',
            'fotos' => 'Фотографии',
            'videolink' => 'Ссылка на видео',
            'date_quest' => 'Дата вопроса',
            'answer' => 'Ответ',
            'user_answer_id' => 'User Answer ID',
            'date_answer' => 'Date Answer',
            'answer_title' => 'Имя арбитра',
            'publish' => 'Публиковать',
            'page'=>'Отдельная страница?'
        ];
    }
}
