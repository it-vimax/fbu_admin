<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Gameswidget;

/**
 * GameswidgetSearch represents the model behind the search form about `app\modules\admin\models\Gameswidget`.
 */
class GameswidgetSearch extends Gameswidget
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'teama_score', 'teamb_score', 'createruserid', 'edituserid'], 'integer'],
            [['teama_name', 'teama_image', 'teamb_name', 'teamb_image', 'ranggame', 'date_game', 'time_game', 'place_game', 'foto_link', 'video_link', 'stats_link', 'post_link', 'youtube_link', 'xssport_link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gameswidget::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'teama_score' => $this->teama_score,
            'teamb_score' => $this->teamb_score,
            'date_game' => $this->date_game,

            'createruserid' => $this->createruserid,
            'edituserid' => $this->edituserid,
        ]);

        $query->andFilterWhere(['like', 'teama_name', $this->teama_name])
            ->andFilterWhere(['like', 'teama_image', $this->teama_image])
            ->andFilterWhere(['like', 'teamb_name', $this->teamb_name])
            ->andFilterWhere(['like', 'teamb_image', $this->teamb_image])
            ->andFilterWhere(['like', 'ranggame', $this->ranggame])
            ->andFilterWhere(['like', 'place_game', $this->place_game])
            ->andFilterWhere(['like', 'foto_link', $this->foto_link])
            ->andFilterWhere(['like', 'video_link', $this->video_link])
            ->andFilterWhere(['like', 'stats_link', $this->stats_link])
            ->andFilterWhere(['like', 'post_link', $this->post_link])
            ->andFilterWhere(['like', 'youtube_link', $this->youtube_link])
            ->andFilterWhere(['like', 'xssport_link', $this->xssport_link]);
        $query->orderBy(['date_game'=>SORT_DESC]);
        return $dataProvider;
    }
}
