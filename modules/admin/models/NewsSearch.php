<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\News;

/**
 * NewsSearch represents the model behind the search form about `app\modules\admin\models\News`.
 */
class NewsSearch extends News
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsid', 'siteid', 'sectionid', 'importanceid', 'isactive', 'isremoved', 'viewcount', 'videotype', 'fototype'], 'integer'],
            [['createuserid', 'title',  'oldurl', 'description', 'fulltext', 'date', 'sourcename', 'sourceurl', 'createdate', 'imagehorizontal', 'edituserid', 'editdate', 'url', 'imagesourcename', 'updatedate', 'nurl'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find()->with(['newsviews']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if(!!$this->nurl){

            $url = explode('/', $this->nurl);
            $url = $url[count($url) - 1 ];
        } else {
            $url ='';
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'newsid' => $this->newsid,
            'siteid' => $this->siteid,
            'sectionid' => $this->sectionid,
            'importanceid' => $this->importanceid,

            'createdate' => $this->createdate,
            'isactive' => $this->isactive,
            'editdate' => $this->editdate,
            'isremoved' => 0,
            'viewcount' => $this->viewcount,
            'updatedate' => $this->updatedate,
            'videotype' => $this->videotype,
            'fototype' => $this->fototype,
        ]);

        $query->andFilterWhere(['like', 'createuserid', $this->createuserid])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'oldurl', $this->oldurl])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'fulltext', $this->fulltext])
            ->andFilterWhere(['like', 'sourcename', $this->sourcename])
            ->andFilterWhere(['like', 'sourceurl', $this->sourceurl])
            ->andFilterWhere(['like', 'imagehorizontal', $this->imagehorizontal])
            ->andFilterWhere(['like', 'edituserid', $this->edituserid])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'imagesourcename', $this->imagesourcename])

            ->andFilterWhere(['like', 'nurl', $url])->orderBy(['date'=>SORT_DESC]);

        return $dataProvider;
    }
}
