<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "section".
 *
 * @property integer $sectionid
 * @property integer $siteid
 * @property string $name
 * @property string $codename
 */
class Section extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sectionid', 'siteid', 'name'], 'required'],
            [['sectionid', 'siteid'], 'integer'],
            [['name', 'codename'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sectionid' => 'Sectionid',
            'siteid' => 'Siteid',
            'name' => 'Name',
            'codename' => 'Codename',
        ];
    }
}
