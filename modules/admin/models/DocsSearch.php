<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Docs;

/**
 * DocsSearch represents the model behind the search form about `app\modules\admin\models\Docs`.
 */
class DocsSearch extends Docs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['docid', 'sort_order', 'catid'], 'integer'],
            [['docname', 'doclink', 'date_added'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Docs::find()->with(['doccategories']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'docid' => $this->docid,
            'date_added' => $this->date_added,
            'sort_order' => $this->sort_order,
            'catid' => $this->catid,
        ]);

        $query->andFilterWhere(['like', 'docname', $this->docname])
            ->andFilterWhere(['like', 'doclink', $this->doclink]);

        return $dataProvider;
    }
}
