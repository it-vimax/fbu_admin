<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "gameswidget".
 *
 * @property integer $id
 * @property string $teama_name
 * @property string $teama_image
 * @property integer $teama_score
 * @property string $teamb_name
 * @property string $teamb_image
 * @property integer $teamb_score
 * @property string $ranggame
 * @property string $date_game
 * @property string $place_game
 * @property string $foto_link
 * @property string $video_link
 * @property string $stats_link
 * @property string $post_link
 * @property string $youtube_link
 * @property string $xssport_link
 * @property integer $createruserid
 * @property integer $edituserid
 */
class Gameswidget extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gameswidget';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teama_name', 'teama_image', 'teamb_name', 'teamb_image', 'ranggame', 'date_game', 'place_game', 'createruserid'], 'required'],
            [['teama_score', 'teamb_score', 'createruserid', 'edituserid', 'active', 'active', 'tourn'], 'integer'],
            [['date_game', 'endgametime'], 'safe'],
            [['foto_link', 'video_link', 'stats_link', 'post_link', 'teama_link', 'teamb_link', 'league_type'], 'string'],
            [['teama_name', 'teamb_name'], 'string', 'max' => 250],
            [['teama_image', 'teamb_image', 'ranggame', 'place_game', 'youtube_link', 'xssport_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teama_name' => 'Название первой команды',
            'teama_image' => 'Логотип первой команды',
            'teama_score' => 'Очки первой команды',
            'teamb_name' => 'Название второй команды',
            'teamb_image' => 'Логотип второй команды',
            'teamb_score' => 'Очки второй команды',
            'ranggame' => 'Название игры',
            'date_game' => 'Дата игры',
            'place_game' => 'Город игры',
            'foto_link' => 'Фотообзор',
            'video_link' => 'Видеообзор',
            'stats_link' => 'Статистика',
            'post_link' => 'Новость',
            'youtube_link' => 'Youtube трансляция',
            'xssport_link' => 'XSSPORT  трансляция',
            'createruserid' => 'Createruserid',
            'edituserid' => 'Edituserid',
            'active' => 'Показывать анонс в топе',
            'endgametime'=>'Время окончания матча',
            'teama_link'=>'Ссылка на сайт первой команды',
            'teamb_link'=>'Ссылка на сайт второй команды',
            'league_type'=>'Соревнование',
            'tourn'=>'Номер тура'
        ];
    }
}
