<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "region_clubs".
 *
 * @property integer $id
 * @property string $team_name
 * @property string $link
 * @property string $logo
 * @property string $site_club
 * @property string $description
 * @property integer $created_user
 * @property string $date_added
 * @property integer $region_id
 */
class RegionClubs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region_clubs';
    }
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_added'],
                ],
                'value' => date("Y-m-d H:i:s"),
            ],



        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_name', 'link', 'logo',  'created_user', 'region_id'], 'required'],
            [['description'], 'string'],
            [['created_user', 'region_id'], 'integer'],
            [['date_added'], 'safe'],
            [['team_name', 'link', 'logo', 'site_club'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_name' => 'Название клуба',
            'link' => 'Ссылка на баскетхотел',
            'logo' => 'Логотип',
            'site_club' => 'Сайт клуба',
            'description' => 'Описание',
            'created_user' => 'Created User',
            'date_added' => 'Date Added',
            'region_id' => 'Region ID',
        ];
    }
}
