<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "newsviews".
 *
 * @property integer $id
 * @property integer $newsid
 * @property integer $count
 * @property string $date
 * @property integer $moved
 */
class Newsviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newsviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsid', 'count', 'date', 'moved'], 'required'],
            [['newsid', 'count', 'moved'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'newsid' => 'Newsid',
            'count' => 'Count',
            'date' => 'Date',
            'moved' => 'Moved',
        ];
    }
}
