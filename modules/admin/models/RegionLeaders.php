<?php

namespace app\modules\admin\models;

use Yii;

use yii\db\ActiveRecord;
use yii\db\Expression;
use zabachok\behaviors\SluggableBehavior;

/**
 * This is the model class for table "region_leaders".
 *
 * @property integer $id
 * @property string $fio
 * @property string $position
 * @property integer $sort_order
 * @property integer $created_user
 * @property string $date_added
 * @property integer $region_id
 */
class RegionLeaders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region_leaders';
    }

     public function behaviors()
    {
        return [
            
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_added'],
                ],
                'value' => date("Y-m-d H:i:s"),
            ],





        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'sort_order', 'created_user', 'region_id'], 'required'],
            [['sort_order', 'created_user', 'region_id'], 'integer'],
            [['date_added'], 'safe'],
            [['fio', 'position', 'foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'foto' => 'Фото',
            'position' => 'Должность',
            'sort_order' => 'Порядок сортировки',
            'created_user' => 'Created User',
            'date_added' => 'Date Added',
            'region_id' => 'Region ID',
        ];
    }
}
