<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $content
 * @property string $date_added
 * @property integer $created_user
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_added'],
                ],
                'value' => date("Y-m-d H:i:s"),
            ],


        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url', 'created_user'], 'required'],
            [['content'], 'string'],
            [['date_added'], 'safe'],
            [['created_user'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'url' => 'url',
            'content' => 'Контент',
            'date_added' => 'Date Added',
            'created_user' => 'Created User',
        ];
    }
}
