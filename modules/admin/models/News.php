<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use zabachok\behaviors\SluggableBehavior;
use app\modules\admin\models\Newsimage;
use app\modules\admin\models\Newsviews;
/**
 * This is the model class for table "news".
 *
 * @property integer $newsid
 * @property integer $siteid
 * @property integer $sectionid
 * @property integer $importanceid
 * @property string $createuserid
 * @property string $title
 * @property string $link
 * @property string $linkattributes
 * @property string $oldurl
 * @property string $description
 * @property string $fulltext
 * @property string $date
 * @property string $sourcename
 * @property string $sourceurl
 * @property string $createdate
 * @property integer $isactive
 * @property string $imagehorizontal
 * @property string $edituserid
 * @property string $editdate
 * @property integer $isremoved
 * @property string $url
 * @property string $imagesourcename
 * @property integer $viewcount
 * @property string $titlefordisplay
 * @property string $updatedate
 * @property string $nurl
 * @property integer $videotype
 * @property integer $fototype
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedate'],
                ],
                'value' => date("Y-m-d H:i:s"),
            ],

            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'nurl',
                'immutable' => true,
                'ensureUnique' => true,

            ],

        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsid', 'siteid', 'sectionid', 'importanceid', 'createuserid', 'title', 'date', 'createdate', 'isactive', 'isremoved', 'viewcount'], 'required'],
            [['newsid', 'siteid', 'sectionid', 'importanceid', 'isactive', 'isremoved', 'viewcount', 'videotype', 'fototype', 'videocat'], 'integer'],
            [['oldurl', 'description', 'fulltext', 'sourceurl', 'url', 'imagesourcename'], 'string'],
            [['date', 'createdate', 'editdate', 'updatedate'], 'safe'],
            [['createuserid', 'edituserid'], 'string', 'max' => 64],
            [['title'], 'string', 'max' => 200],
            [['sourcename'], 'string', 'max' => 100],
            [['imagehorizontal'], 'string', 'max' => 250],
            [['nurl'], 'string', 'max' => 255],
            [['newsid'], 'unique'],
            [['nurl'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newsid' => 'Newsid',
            'siteid' => 'Сайт',
            'sectionid' => 'Раздел',
            'importanceid' => 'Важность',
            'createuserid' => 'Создал новость',
            'title' => 'Заголовок',
            'oldurl' => 'Oldurl',
            'description' => 'Краткое описание',
            'fulltext' => 'Описание',
            'date' => 'Дата публикации',
            'sourcename' => 'Источник',
            'sourceurl' => 'Ссылка на источник',
            'createdate' => 'Createdate',
            'isactive' => 'Опубликовать',
            'imagehorizontal' => 'Картинка',
            'edituserid' => 'Редактировал новость',
            'editdate' => 'Время последнего редактирования',
            'isremoved' => 'Isremoved',
            'url' => 'Url',
            'imagesourcename' => 'Ссылка на источник картинки',
            'viewcount' => 'Viewcount',
            'updatedate' => 'Updatedate',
            'nurl' => 'Ссылка',
            'videotype' => 'Видео новость',
            'newsviews'=>'Количество просмотров',
            'fototype' => 'Фото новость',
            'videocat'=>'Категория видео'
        ];
    }
    public function getNewsimage(){
        return $this->hasMany(Newsimage::className(), ['newsimage'=>'newsimage']);
    }

    public function getNewsviews(){
        return $this->hasOne(Newsviews::className(), ['newsid'=>'newsid']);
    }
}
