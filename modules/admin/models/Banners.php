<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "banners".
 *
 * @property integer $id
 * @property string $foto
 * @property integer $link
 * @property integer $created_user
 * @property string $date_added
 */
class Banners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banners';
    }
     public function behaviors()
    {
        return [
           
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_added'],
                ],
                'value' => date("Y-m-d H:i:s"),
            ],





        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['foto', 'created_user'], 'required'],
            [['created_user', 'position'], 'integer'],
            [['date_added'], 'safe'],
            [['foto', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'foto' => 'Фото',
            'link' => 'Ссылка',
            'created_user' => 'Created User',
            'date_added' => 'Date Added',
        ];
    }
}
