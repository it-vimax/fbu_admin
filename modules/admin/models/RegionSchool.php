<?php

namespace app\modules\admin\models;


use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use zabachok\behaviors\SluggableBehavior;

/**
 * This is the model class for table "region_school".
 *
 * @property integer $id
 * @property string $name
 * @property string $contact
 * @property string $description
 * @property integer $separet_page
 * @property integer $url
 * @property integer $created_user
 * @property string $date_added
 * @property integer $region_id
 */
class RegionSchool extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region_school';
    }
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'url',
                'immutable' => true,
                'ensureUnique' => true,

            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_added'],
                ],
                'value' => date("Y-m-d H:i:s"),
            ],





        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'created_user', 'region_id'], 'required'],
            [['contact', 'description', 'url'], 'string'],
            [['separet_page', 'created_user', 'region_id'], 'integer'],
            [['date_added'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название школы',
            'contact' => 'Контакты',
            'description' => 'Описание',
            'separet_page' => 'Отдельная страница',
            'url' => 'Url',
            'created_user' => 'Craeted User',
            'date_added' => 'Date Added',
            'region_id' => 'Region ID',
        ];
    }
}
