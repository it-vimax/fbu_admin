<?php

namespace app\modules\admin\models;

use Yii;
use zabachok\behaviors\SluggableBehavior;
/**
 * This is the model class for table "topic".
 *
 * @property integer $topicid
 * @property string $name
 * @property string $codename
 * @property string $fulltext
 * @property integer $siteid
 * @property integer $createuserid
 */
class Topic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'topic';
    }
    public function behaviors()
    {
        return [


            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'codename',
                'immutable' => true,
                'ensureUnique' => true,

            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['siteid'], 'required'],
            [['topicid', 'siteid', 'createuserid'], 'integer'],
            [['fulltext'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['codename'], 'string', 'max' => 50],
            [['codename'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'topicid' => 'Топик',
            'name' => 'Название',
            'codename' => 'Codename',
            'fulltext' => 'Описание',
            'siteid' => 'Сайт',
            'createuserid' => 'Createuserid',
        ];
    }
}
