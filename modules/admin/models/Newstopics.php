<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "newstopics".
 *
 * @property integer $newsid
 * @property integer $topicid
 * @property integer $sort_order
 */
class Newstopics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newstopics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsid', 'topicid'], 'required'],
            [['newsid', 'topicid', 'sort_order'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newsid' => 'Newsid',
            'topicid' => 'Topicid',
            'sort_order' => 'Sort Order',
        ];
    }
}
