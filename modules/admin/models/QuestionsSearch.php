<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Questions;

/**
 * QuestionsSearch represents the model behind the search form about `app\modules\admin\models\Questions`.
 */
class QuestionsSearch extends Questions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_answer_id', 'publish'], 'integer'],
            [['question', 'fotos', 'videolink', 'date_quest', 'answer', 'date_answer', 'answer_title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Questions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_quest' => $this->date_quest,
            'user_answer_id' => $this->user_answer_id,
            'date_answer' => $this->date_answer,
            'publish' => $this->publish,
        ]);

        $query->andFilterWhere(['like', 'question', $this->question])
            ->andFilterWhere(['like', 'fotos', $this->fotos])
            ->andFilterWhere(['like', 'videolink', $this->videolink])
            ->andFilterWhere(['like', 'answer', $this->answer])
            ->andFilterWhere(['like', 'answer_title', $this->answer_title])
            ->orderBy(['date_quest'=>SORT_DESC]);
        return $dataProvider;
    }
}
