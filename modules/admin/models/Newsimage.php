<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "newsimage".
 *
 * @property integer $newsimageid
 * @property integer $newsid
 * @property string $filename
 * @property string $title
 * @property integer $position
 * @property string $sourcename
 */
class Newsimage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newsimage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsid', 'position'], 'required'],
            [['newsimageid', 'newsid', 'position'], 'integer'],
            [['title', 'sourcename'], 'string'],
            [['filename'], 'string', 'max' => 250],
            [['newsimageid'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newsimageid' => 'Newsimageid',
            'newsid' => 'Newsid',
            'filename' => 'Картинка',
            'title' => 'Title',
            'position' => 'Позиция',
            'sourcename' => 'Источник',
        ];
    }
}
