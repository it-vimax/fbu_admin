<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "topic".
 *
 * @property integer $topicid
 * @property string $name
 * @property string $codename
 * @property string $fulltext
 * @property integer $siteid
 * @property integer $createuserid
 */
class Topictag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topicid', 'siteid'], 'required'],
            [['topicid', 'siteid', 'createuserid'], 'integer'],
            [['fulltext'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['codename'], 'string', 'max' => 50],
            [['codename'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'topicid' => 'Топик',
            'name' => 'Название',
            'codename' => 'Codename',
            'fulltext' => 'Описание',
            'siteid' => 'Сайт',
            'createuserid' => 'Createuserid',
        ];
    }
}
