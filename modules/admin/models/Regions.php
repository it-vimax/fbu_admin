<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use zabachok\behaviors\SluggableBehavior;

/**
 * This is the model class for table "regions".
 *
 * @property integer $region_id
 * @property string $title
 * @property string $site_link
 * @property string $contacts
 * @property string $url
 * @property integer $created_user
 * @property string $date_added
 * @property integer $modificated_user
 * @property string $date_modificated
 */
class Regions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regions';
    }
    public function behaviors()
    {
        return [
            'slug' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'url',
                'immutable' => true,
                'ensureUnique' => true,

            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_added'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['date_modificated'],
                ],
                'value' => date("Y-m-d H:i:s"),
            ],



        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'title', 'contacts', 'created_user'], 'required'],
            [['region_id', 'created_user', 'modificated_user', 'topicid'], 'integer'],
            [['contacts', 'amatour_link'], 'string'],
            [['date_added', 'date_modificated'], 'safe'],
            [['title', 'site_link', 'url', 'amatour_link'], 'string', 'max' => 255],
            [['url'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'region_id' => 'Регион',
            'title' => 'Заголовок',
            'site_link' => 'Сайт',
            'contacts' => 'Контакты',
            'url' => 'Url',
            'created_user' => 'Created User',
            'date_added' => 'Date Added',
            'modificated_user' => 'Modificated User',
            'date_modificated' => 'Date Modificated',
            'topicid'=>'Тег',
            'amatour_link'=>'Ссылка на аматорский чемпионат'
        ];
    }
}
