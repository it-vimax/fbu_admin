<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;
use app\modules\admin\models\Doccategories;
/**
 * This is the model class for table "docs".
 *
 * @property integer $docid
 * @property string $docname
 * @property string $doclink
 * @property string $date_added
 * @property integer $sort_order
 * @property integer $catid
 */
class Docs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'docs';
    }

        public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_added'],
                ],
                'value' => date("Y-m-d H:i:s"),
            ],

           

        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['docname', 'doclink', 'catid', 'sort_order'], 'required'],
            [['doclink'], 'string'],
            [['date_added'], 'safe'],
            [['sort_order', 'catid'], 'integer'],
            [['docname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'docid' => 'Docid',
            'docname' => 'Название документа',
            'doclink' => 'Документ',
            'date_added' => 'Дата добавления',
            'sort_order' => 'Порядок сортировки',
            'catid' => 'Категория',
            'doccategories'=>'Категория',
        ];
    }
    public function getDoccategories(){
        return $this->hasOne(Doccategories::className(), ['doccatid'=>'catid']);
    }
}
