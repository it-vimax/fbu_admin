<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use kartik\file\FileInput;

$this->title = Yii::t('imagemanager','Image manager');

?>
<div id="module-imagemanager" class="container-fluid <?=$selectType?>">
	<div class="row">
		<div class="col-xs-6 col-sm-10 col-image-editor">
			<div class="image-cropper">
				<div class="image-wrapper">
					<img id="image-cropper" />
				</div>
				<div class="action-buttons">
					<a href="#" class="btn btn-primary apply-crop">
						<i class="fa fa-crop"></i>
						<span class="hidden-xs"><?=Yii::t('imagemanager','Crop')?></span>
					</a>
					<?php if($viewMode === "iframe"): ?>
					<a href="#" class="btn btn-primary apply-crop-select">
						<i class="fa fa-crop"></i>
						<span class="hidden-xs"><?=Yii::t('imagemanager','Crop and select')?></span>
					</a>
					<?php endif; ?>
					<a href="#" class="btn btn-default cancel-crop">
						<i class="fa fa-undo"></i>
						<span class="hidden-xs"><?=Yii::t('imagemanager','Cancel')?></span>
					</a>
				</div>
			</div> 
		</div>
		<div class="col-xs-6 col-sm-10 col-overview">
			<div class="form-group">

					<?php Pjax::begin([
						'id'=>'pjax-mediamanager',
						'timeout'=>'5000'
					]); ?>


					<div id="w0" class="list-view">
						<div class="item-overview">
						<?php for($i = 2 ; $i < count($items); $i++) { ?>
							<?php if(!is_dir($path.'/'.$items[$i])){ ?>
								<div class="item img-thumbnail" data-key="<?=$items[$i]?>"><div class="thumbnail">
										<img src="<?=$path.'/'.$items[$i]?>" alt="<?=$items[$i]?>">
										<div class="filename"><?=$items[$i]?></div>
									</div>
								</div>
							<?php	}	 ?>


						<?php } ?>
						</div>
					</div>
				<?php Pjax::end(); ?>
			</div>
		</div>
		<div class="col-xs-6 col-sm-2 col-options">


			<?php
				if (Yii::$app->controller->module->canUploadImage):
			?>

			<?=FileInput::widget([
				'name' => 'imagemanagerFiles[]',
				'id' => 'imagemanager-files',
				'options' => [
					'multiple' => true,
					'accept' => 'image/*',
					'news-id'=>$news_id
				],
				'pluginOptions' => [
					'uploadUrl' => Url::to(['manager/upload', 'news_id'=>$news_id]),
					'allowedFileExtensions' => \Yii::$app->controller->module->allowedFileExtensions, 
					'uploadAsync' => false,
					'showPreview' => false,
					'showRemove' => false,
					'showUpload' => false,
					'showCancel' => false,
					'browseClass' => 'btn btn-primary btn-block',
					'browseIcon' => '<i class="fa fa-upload"></i> ',
					'browseLabel' => Yii::t('imagemanager','Upload')
				],
				'pluginEvents' => [
					"filebatchselected" => "function(event, files){  $('.msg-invalid-file-extension').addClass('hide'); $(this).fileinput('upload'); }",
					"filebatchuploadsuccess" => "function(event, data, previewId, index) {
						imageManagerModule.uploadSuccess(data.jqXHR.responseJSON.imagemanagerFiles);
					}",
					"fileuploaderror" => "function(event, data) { $('.msg-invalid-file-extension').removeClass('hide'); }",
				],
			]) ?>

			<?php
				endif;
			?>
		<div class="image-info"><a href="#" class="btn btn-primary btn-block pick-all-image" > Отметить все</a></div>

			<div class="image-info hide">
				<div class="thumbnail">
					<img src="#">
				</div>
				<!--<div class="edit-buttons">
					<a href="#" class="btn btn-primary btn-block crop-image-item">
						<i class="fa fa-crop"></i>
						<span class="hidden-xs"><?=Yii::t('imagemanager','Crop')?></span>
					</a>
				</div>-->
				<div class="details">
					<div class="fileName"></div>
					<div class="created"></div>
					<div class="fileSize"></div>
					<!--<div class="dimensions"><span class="dimension-width"></span> &times; <span class="dimension-height"></span></div>-->
					<?php
						if (Yii::$app->controller->module->canRemoveImage):
					?>
						<a href="#" class="btn btn-xs btn-danger delete-image-item" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <?=Yii::t('imagemanager','Delete')?></a>
					<?php
						endif;
					?>
				</div>
				<?php if($viewMode === "iframe"): ?>
				<a href="#" class="btn btn-primary btn-block pick-image-item"><?=Yii::t('imagemanager','Select')?></a> 
				<?php endif; ?>
			</div>
		</div>  
	</div>
</div>  