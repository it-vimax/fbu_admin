<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'FBU',
        'brandUrl' => '/admin',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [

            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
</div>
    <div class="container-fluid content-main">
        <div class="row">
            <div class="col-sm-1 column-left">
                <div class="menu_left">
                    <div class="hide_menu">

                        <ul>
                            <li><a href="<?=Url::to(['/admin/news/index'])?>"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Новости</a></li>
                            <li><a href="<?=Url::to(['/admin/topic'])?>"><i class="fa fa-tags" aria-hidden="true"></i> Топики</a></li>
                            <li><a href="<?=Url::to(['/admin/gameswidget'])?>"><i class="fa fa-futbol-o" aria-hidden="true"></i> Виджет игр</a></li>
                            <li><a href="<?=Url::to(['/admin/regions'])?>"><i class="fa fa-map-o" aria-hidden="true"></i> Регионы</a></li>
                            <li><a href="<?=Url::to(['/admin/banners'])?>"><i class="fa fa-camera" aria-hidden="true"></i> Афиши</a></li>

                            <li><a href="<?=Url::to(['/admin/pages'])?>"><i class="fa fa-file-o" aria-hidden="true"></i> Страницы</a></li>
                             <li><a href="<?=Url::to(['/admin/questions'])?>"><i class="fa fa-question-circle-o" aria-hidden="true"></i> Вопросы арбитрам</a></li>
                            <li><a href="<?=Url::to(['/rbac/user'])?>"><i class="fa fa-users" aria-hidden="true"></i> Пользователи</a></li>
                           
                            <li><a href="<?=Url::to(['/admin/doccategories'])?>"><i class="fa fa-file-o" aria-hidden="true"></i> Категории документов</a></li>
                            <li><a href="<?=Url::to(['/admin/docs'])?>"><i class="fa fa-file-o" aria-hidden="true"></i> Документы</a></li>

                            <li><a href="<?=Url::to(['/history'])?>"><i class="fa fa-file-o" aria-hidden="true"></i> История баскетбола</a></li>

                            <li><a href="<?=Url::to(['/admin/default/clear'])?>"><i class="fa fa-shower" aria-hidden="true"></i> Очистить кеш сайта</a></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-sm-11 column-right">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </div>
        </div>
    </div>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
